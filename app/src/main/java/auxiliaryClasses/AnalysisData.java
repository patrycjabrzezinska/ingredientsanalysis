package auxiliaryClasses;

import java.util.ArrayList;

public class AnalysisData {

    private ArrayList<String> antiAgingProducts = new ArrayList<>();
    private ArrayList<String> eyeCreamProducts = new ArrayList<>();
    private ArrayList<String> faceMaskProducts = new ArrayList<>();
    private ArrayList<String> faceMoisturiserProducts = new ArrayList<>();
    private ArrayList<String> footCreamLotionProducts = new ArrayList<>();
    private ArrayList<String> handCreamLotionProducts = new ArrayList<>();
    private ArrayList<String> conditionerProducts = new ArrayList<>();
    private ArrayList<String> hairSprayProducts = new ArrayList<>();
    private ArrayList<String> showerGelProducts = new ArrayList<>();
    private ArrayList<String> hairDyeProducts = new ArrayList<>();
    private ArrayList<String> shampooProducts = new ArrayList<>();
    private ArrayList<String> sunCreamProducts = new ArrayList<>();
    private ArrayList<String> lipBalmCreamProducts = new ArrayList<>();

    public boolean checkIfAlreadyAnalysed(String category){
        if(category.equals("Anti aging")){
            return !antiAgingProducts.isEmpty();
        }
        else if(category.equals("Eye cream")){
            return !eyeCreamProducts.isEmpty();
        }
        else if(category.equals("Face mask")){
            return !faceMaskProducts.isEmpty();
        }
        else if(category.equals("Face moisturiser")){
            return !faceMoisturiserProducts.isEmpty();
        }
        else if(category.equals("Foot cream&lotion")){
            return !footCreamLotionProducts.isEmpty();
        }
        else if(category.equals("Hand cream&lotion")){
            return !handCreamLotionProducts.isEmpty();
        }
        else if(category.equals("Conditioner")){
            return !conditionerProducts.isEmpty();
        }
        else if(category.equals("Hair spray")){
            return !hairSprayProducts.isEmpty();
        }
        else if(category.equals("Shower gel")){
            return !showerGelProducts.isEmpty();
        }
        else if(category.equals("Hair dye")){
            return !hairDyeProducts.isEmpty();
        }
        else if(category.equals("Shampoo")){
            return !shampooProducts.isEmpty();
        }
        else if(category.equals("Sun cream")){
            return !sunCreamProducts.isEmpty();
        }
        else if(category.equals("Lip balm&cream")){
            return !lipBalmCreamProducts.isEmpty();
        }
        else{
            return false;
        }
    }

    public void saveAnalysedProducts(String category, ArrayList<String> analysedProducts){
        if(category.equals("Anti aging")){
            antiAgingProducts.addAll(analysedProducts);
        }
        else if(category.equals("Eye cream")){
            eyeCreamProducts.addAll(analysedProducts);
        }
        else if(category.equals("Face mask")){
            faceMaskProducts.addAll(analysedProducts);
        }
        else if(category.equals("Face moisturiser")){
            faceMoisturiserProducts.addAll(analysedProducts);
        }
        else if(category.equals("Foot cream&lotion")){
            footCreamLotionProducts.addAll(analysedProducts);
        }
        else if(category.equals("Hand cream&lotion")){
            handCreamLotionProducts.addAll(analysedProducts);
        }
        else if(category.equals("Conditioner")){
            conditionerProducts.addAll(analysedProducts);
        }
        else if(category.equals("Hair spray")){
            hairSprayProducts.addAll(analysedProducts);
        }
        else if(category.equals("Shower gel")){
            showerGelProducts.addAll(analysedProducts);
        }
        else if(category.equals("Hair dye")){
            hairDyeProducts.addAll(analysedProducts);
        }
        else if(category.equals("Shampoo")){
            shampooProducts.addAll(analysedProducts);
        }
        else if(category.equals("Sun cream")){
            sunCreamProducts.addAll(analysedProducts);
        }
        else if(category.equals("Lip balm&cream")){
            lipBalmCreamProducts.addAll(analysedProducts);
        }
    }

    public ArrayList<String> getAnalysedProductsFromCategory(String category){
        if(category.equals("Anti aging")){
            return antiAgingProducts;
        }
        else if(category.equals("Eye cream")){
            return eyeCreamProducts;
        }
        else if(category.equals("Face mask")){
            return faceMaskProducts;
        }
        else if(category.equals("Face moisturiser")){
            return faceMoisturiserProducts;
        }
        else if(category.equals("Foot cream&lotion")){
            return footCreamLotionProducts;
        }
        else if(category.equals("Hand cream&lotion")){
            return handCreamLotionProducts;
        }
        else if(category.equals("Conditioner")){
            return conditionerProducts;
        }
        else if(category.equals("Hair spray")){
            return hairSprayProducts;
        }
        else if(category.equals("Shower gel")){
            return showerGelProducts;
        }
        else if(category.equals("Hair dye")){
            return hairDyeProducts;
        }
        else if(category.equals("Shampoo")){
            return shampooProducts;
        }
        else if(category.equals("Sun cream")){
            return sunCreamProducts;
        }
        else if(category.equals("Lip balm&cream")){
            return lipBalmCreamProducts;
        }
        else {
            return null;
        }
    }

    public void clearAll(){
        antiAgingProducts.clear();
        eyeCreamProducts.clear();
        faceMaskProducts.clear();
        faceMoisturiserProducts.clear();
        footCreamLotionProducts.clear();
        handCreamLotionProducts.clear();
        conditionerProducts.clear();
        hairSprayProducts .clear();
        showerGelProducts.clear();
        hairDyeProducts.clear();
        shampooProducts.clear();
        sunCreamProducts.clear();
        lipBalmCreamProducts.clear();
    }
}
