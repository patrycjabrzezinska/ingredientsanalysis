package auxiliaryClasses;

import java.util.ArrayList;
import java.util.List;

public class Ingredient {
    private String INCIname;
    private int ifParaben;
    private int ifAllergen;
    private int ifPopularAllergen;
    private int ifAnimalOrigin;
    private int ifBenzylDerivative;
    private int ifAlcohol;
    private int ifSilicone;
    private int ifDangerousForPregnant;
    private int influenceOnOilySkin;
    private int influenceOnDrySkin;
    private int influenceOnCombinationSkin;
    private int influenceOnAcneProne;
    private int influenceOnSensitiveSkin;
    private int influenceOnDryHair;
    private int influenceOnOilyHair;
    private int influenceOnDandruff;
    private int influenceOnAtopicDermatitis;
    private double points =0;
    private int numberOfGoods =0;
    private int numberOfBads =0;
    private int numberOfNeutrals =0;
    private boolean ifUserAllergen = false;
    List<String> ingredientInfo = new ArrayList<String>();



    public Ingredient(String INCIname, int ifParaben, int ifAllergen, int ifPopularAllergen,
                      int ifAnimalOrigin, int ifBenzylDerivative, int ifAlcohol, int ifSilicone,
                      int ifDangerousForPregnant, int influenceOnOilySkin, int influenceOnDrySkin,
                      int influenceOnCombinationSkin, int influenceOnAcneProne, int influenceOnSensitiveSkin,
                      int influenceOnDryHair, int influenceOnOilyHair, int influenceOnDandruff, int influenceOnAtopicDermatitis) {
        this.INCIname = INCIname;
        this.ifParaben = ifParaben;
        this.ifAllergen = ifAllergen;
        this.ifPopularAllergen = ifPopularAllergen;
        this.ifAnimalOrigin = ifAnimalOrigin;
        this.ifBenzylDerivative = ifBenzylDerivative;
        this.ifAlcohol = ifAlcohol;
        this.ifSilicone = ifSilicone;
        this.ifDangerousForPregnant = ifDangerousForPregnant;
        this.influenceOnOilySkin = influenceOnOilySkin;
        this.influenceOnDrySkin = influenceOnDrySkin;
        this.influenceOnCombinationSkin = influenceOnCombinationSkin;
        this.influenceOnAcneProne = influenceOnAcneProne;
        this.influenceOnSensitiveSkin = influenceOnSensitiveSkin;
        this.influenceOnDryHair = influenceOnDryHair;
        this.influenceOnOilyHair = influenceOnOilyHair;
        this.influenceOnDandruff = influenceOnDandruff;
        this.influenceOnAtopicDermatitis = influenceOnAtopicDermatitis;
    }

    public void countSkinType(String userSkinType){

        if(userSkinType.equals("oily")){
            if(influenceOnOilySkin==0){
                ingredientInfo.add("Bad influence on your oily skin :(");
                numberOfBads++;
            }
            else if(influenceOnOilySkin==2){
                ingredientInfo.add("Good influence on your oily skin :)");
                points = points + 5*2;
                numberOfGoods++;
            }
            else {
                points = points+1*1;
                numberOfNeutrals++;
            }
        }
        else if(userSkinType.equals("dry")){
            if(influenceOnDrySkin==0){
                ingredientInfo.add("Bad influence on your dry skin :(");
                numberOfBads++;
            }
            else if(influenceOnDrySkin==2){
                ingredientInfo.add("Good influence on your dry skin :)");
                points = points + 5*2;
                numberOfGoods++;
            }
            else{
                points = points+1*1;
                numberOfNeutrals++;
            }
        }
        else if(userSkinType.equals("sensitive")){
            if(influenceOnSensitiveSkin==0){
                ingredientInfo.add("Bad influence on your sensitive skin :(");
                numberOfBads++;
            }
            else if(influenceOnSensitiveSkin==2){
                ingredientInfo.add("Good influence on your sensitive skin :)");
                points = points + 5*2;
                numberOfGoods++;
            }
            else{
                points = points+1*1;
                numberOfNeutrals++;
            }
        }
        else if(userSkinType.equals("combination")){
            if(influenceOnCombinationSkin==0){
                ingredientInfo.add("Bad influence on your combination skin :(");
                numberOfBads++;
            }
            else if(influenceOnCombinationSkin==2){
                ingredientInfo.add("Good influence on your combination skin :)");
                points = points + 5*2;
                numberOfGoods++;
            }
            else{
                points = points+1*1;
                numberOfNeutrals++;
            }
        }

    }

    public void countHairType(String userHairType){
        if(userHairType.equals("greasy")){
            if(influenceOnOilyHair==0){
                ingredientInfo.add("Bad influence on your greasy hair :(");
                numberOfBads++;
            }
            else if(influenceOnOilyHair==2){
                ingredientInfo.add("Good influence on your greasy hair :)");
                points = points + 5*2;
                numberOfGoods++;
            }
            else {
                points = points+1*1;
                numberOfNeutrals++;
            }
        }
        else if(userHairType.equals("dry")){
            if(influenceOnDryHair==0){
                ingredientInfo.add("Bad influence on your dry hair :(");
                numberOfBads++;
            }
            else if(influenceOnDryHair==2){
                ingredientInfo.add("Good influence on your dry hair :)");
                points = points + 5*2;
                numberOfGoods++;
            }
            else{
                points = points+1*1;
                numberOfNeutrals++;
            }
        }
    }

    public void countIfAcne(){
        if(influenceOnAcneProne==0){
            ingredientInfo.add("Bad influence on your acne prone skin :(");
            numberOfBads++;
        }
        else if(influenceOnAcneProne==2){
            ingredientInfo.add("Good influence on your acne prone skin :)");
            points = points+5*2;
            numberOfGoods++;
        }
        else{
            points = points+1*1;
            numberOfNeutrals++;
        }
    }

    public void countIfDermatitis(){
        if(influenceOnAtopicDermatitis==0){
            ingredientInfo.add("Bad influence on your atopic dermatitis :(");
            numberOfBads++;
        }
        else if(influenceOnAtopicDermatitis==2){
            ingredientInfo.add("Good influence on your atopic dermatitis :)");
            points = points+5*2;
            numberOfGoods++;
        }
        else{
            points = points +1*1;
            numberOfNeutrals++;
        }
    }

    public void countIfDandruff(){
        if(influenceOnDandruff==0){
            ingredientInfo.add("Bad influence on your dandruff :(");
            numberOfBads++;
        }
        else if(influenceOnDandruff==2){
            ingredientInfo.add("Good influence on your dandruff :)");
            points = points+5*2;
            numberOfGoods++;
        }
        else{
            points = points+1*1;
            numberOfNeutrals++;
        }
    }

    public void countIfAlcohol(){
        if (ifAlcohol == 1) {
            ingredientInfo.add("This ingredient is an alcohol :(");
            numberOfBads++;
        } else {
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIfSilicone(){
        if(ifSilicone==1){
            ingredientInfo.add("This ingredient is a silicone :(");
            numberOfBads++;
        }
        else{
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIfBenzylDerivative(){
        if(ifBenzylDerivative==1){
            ingredientInfo.add("This ingredient is a benzyl derivative :(");
            numberOfBads++;
        }
        else{
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIfParaben(){
        if(ifParaben==1){
            ingredientInfo.add("This ingredient is a paraben :(");
            numberOfBads++;
        }
        else{
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIfAnimalOrigin(){
        if(ifAnimalOrigin==1){
            ingredientInfo.add("This ingredient is animal origin :(");
            numberOfBads++;
        }
        else{
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIfPopularAllergen(){
        if(ifPopularAllergen==1){
            ingredientInfo.add("This ingredient is a popular allergen :(");
            numberOfBads++;
        }
        else{
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIfAllAllergen(){
        if(ifAllergen==1){
            ingredientInfo.add("This ingredient is an allergen :(");
            numberOfBads++;
        }
        else{
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIfDangerousForPregnant(){
        if(ifDangerousForPregnant==1){
            ingredientInfo.add("This ingredient is dangerous for pregnant :(");
            numberOfBads++;
        }
        else{
            numberOfNeutrals++;
            points = points + 1 * 1;
        }
    }

    public void countIngredientAverage(){
        points = points/(numberOfGoods*5+numberOfNeutrals+numberOfBads*30);
    }

    public boolean checkIfUserAllergen(ArrayList<String> avoidAllergen){
        for(int i =0;i<avoidAllergen.size();i++){
            if(avoidAllergen.get(i).equals(INCIname)){
                ifUserAllergen = true;
                return true;
            }
        }
        return false;
    }

    public String getINCIname() {
        return INCIname;
    }

    public int getIfParaben() {
        return ifParaben;
    }

    public int getIfAllergen() {
        return ifAllergen;
    }

    public int getIfPopularAllergen() {
        return ifPopularAllergen;
    }

    public int getIfAnimalOrigin() {
        return ifAnimalOrigin;
    }

    public int getIfBenzylDerivative() {
        return ifBenzylDerivative;
    }

    public int getIfAlcohol() {
        return ifAlcohol;
    }

    public int getIfSilicone() {
        return ifSilicone;
    }

    public int getIfDangerousForPregnant() {
        return ifDangerousForPregnant;
    }

    public int getInfluenceOnOilySkin() {
        return influenceOnOilySkin;
    }

    public int getInfluenceOnDrySkin() {
        return influenceOnDrySkin;
    }

    public int getInfluenceOnCombinationSkin() {
        return influenceOnCombinationSkin;
    }

    public int getInfluenceOnAcneProne() {
        return influenceOnAcneProne;
    }

    public int getInfluenceOnSensitiveSkin() {
        return influenceOnSensitiveSkin;
    }

    public int getInfluenceOnDryHair() {
        return influenceOnDryHair;
    }

    public int getInfluenceOnOilyHair() {
        return influenceOnOilyHair;
    }

    public int getInfluenceOnDandruff() {
        return influenceOnDandruff;
    }

    public int getInfluenceOnAtopicDermatitis() {
        return influenceOnAtopicDermatitis;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public int getNumberOfGoods() {
        return numberOfGoods;
    }

    public int getNumberOfBads() {
        return numberOfBads;
    }

    public int getNumberOfNeutrals() {
        return numberOfNeutrals;
    }

    public List<String> getIngredientInfo() {
        return ingredientInfo;
    }

    public boolean isIfUserAllergen() {
        return ifUserAllergen;
    }

}

