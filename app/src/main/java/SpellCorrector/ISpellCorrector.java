package SpellCorrector;

import java.io.IOException;
import java.io.InputStream;

public interface ISpellCorrector {
	void useDictionary(InputStream dictionaryFileName) throws IOException;
	String suggestSimilarWord(String inputWord);
}
