package com.example.cosmeticspy;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import auxiliaryClasses.AnalysisData;


public class HomePageActivity extends AppCompatActivity{

    ListView listView;
    ArrayList<String> products = new ArrayList<>();
    ArrayList<String> all_products = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>(Arrays.asList("Anti aging",
        "Eye cream", "Face mask", "Face moisturiser","Body moisturiser", "Foot cream&lotion", "Hand cream&lotion", "Conditioner",
        "Hair spray", "Shower gel", "Hair dye", "Shampoo", "Sun cream", "Lip balm&cream"));
    ArrayList<String> ingredients = new ArrayList<>();
    SearchAdapter arrayAdapter = null;
    String selected_category;
    LinearLayout optionsLayout;
    EditText editSearch;
    Button buttonIngredients;
    Button buttonProducts;
    Spinner spinner;
    boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives, ifParabens, ifPopularAllergens, ifAllAllergens, ifAnimalOrigin, ifDangerousForPregnant;
    String skinType, hairType;
    ArrayList<String> avoidAllergens = new ArrayList<>();
    int layout_height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        listView = findViewById(R.id.listView);
        editSearch = findViewById(R.id.editSearch);
        buttonIngredients = (Button)findViewById(R.id.button_ingredients);
        buttonProducts = (Button)findViewById(R.id.button_products);
        spinner = (Spinner) findViewById(R.id.category_spiner);
        optionsLayout = (LinearLayout) findViewById(R.id.options_layout);
        getUserInformation();
        listView.setTextFilterEnabled(true);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.getMenu().getItem(1).setChecked(true);
        ArrayAdapter<String> category_adapter = new ArrayAdapter<>(HomePageActivity.this, android.R.layout.simple_spinner_item, categories);
        category_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        buttonProducts.setBackgroundColor(ContextCompat.getColor(HomePageActivity.this, R.color.blue_pressed));
        spinner.setAdapter(category_adapter);
        layout_height = optionsLayout.getLayoutParams().height;

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long id) {
                selected_category = arg0.getItemAtPosition(pos).toString();
                load_products();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        buttonIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonProducts.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.our_blue));
                buttonIngredients.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.blue_pressed));
                slideView(optionsLayout, optionsLayout.getLayoutParams().height, 0);
                search_ingredient();
            }
        });
        buttonProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    buttonProducts.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.blue_pressed));
                    buttonIngredients.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.our_blue));
                    slideView(optionsLayout, optionsLayout.getLayoutParams().height, layout_height);
                    load_products();

            }
        });


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_search:
                        Intent intent_search = new Intent(HomePageActivity.this, HomePageActivity.class);
                        startActivity(intent_search);
                        break;
                    case R.id.action_camera:
                        Intent intent_camera = new Intent(HomePageActivity.this, CameraInstructionsActivity.class);
                        startActivity(intent_camera);
                        break;
                    case R.id.action_account:
                        Intent intent_account = new Intent(HomePageActivity.this, UserAccountActivity.class);
                        startActivity(intent_account);
                        break;
                }
                return true;
            }
        });
    }

    public void search_ingredient() {
        final App application = (App) getApplicationContext();
        ArrayList<ParseObject> allIngredients = application.getAllIngredientsAll();

        for(ParseObject ingredient : allIngredients){
            ingredients.add(ingredient.getString("INCIname"));
        }
            if(arrayAdapter != null){
                arrayAdapter.clear();
            }

        arrayAdapter = new SearchAdapter(this, 0, ingredients){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row;

                if (null == convertView) {
                    row = getLayoutInflater().inflate(android.R.layout.simple_list_item_1, null);
                } else {
                    row = convertView;
                }
                TextView tv = (TextView) row.findViewById(android.R.id.text1);
                tv.setText(Html.fromHtml((String) getItem(position)));
                return row;
            }
        };
        listView.setAdapter(arrayAdapter);

            editSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String[] separated_term = s.toString().split(" ");
                    StringBuilder term= new StringBuilder();
                    for(int i=0; i<separated_term.length; i++){
                        if(!separated_term[i].equals("")){
                            term.append(separated_term[i]);
                            if(i<separated_term.length-1){
                                term.append(" ");
                            }
                        }
                    }
                    arrayAdapter.getFilter().filter(term.toString());
                    arrayAdapter.notifyDataSetChanged();

                    listView.setOnItemClickListener((parent, view, position, id) -> {
                        Intent intent_ingredient_details = new Intent(view.getContext(), IngredientDetailsActivity.class);
                        intent_ingredient_details.putExtra("INCIname", arrayAdapter.getItem(position));
                        startActivity(intent_ingredient_details);
                    });

                }
                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            listView.setOnItemClickListener((parent, view, position, id) -> {
                Intent intent_ingredient_details = new Intent(view.getContext(), IngredientDetailsActivity.class);
                intent_ingredient_details.putExtra("INCIname", ingredients.get(position));
                startActivity(intent_ingredient_details);
            });

    }

    public void search_product2(String selected_category) throws ParseException, InterruptedException {
        products.clear();
        final App application = (App) getApplicationContext();
        AnalysisData analysisData = application.getAnalysisData();
        if(analysisData.checkIfAlreadyAnalysed(selected_category) && ParseUser.getCurrentUser().getUpdatedAt().toString().equals(application.getUserLastUpdate()) && !application.isIfAllergyUpdate()){
            products.addAll(analysisData.getAnalysedProductsFromCategory(selected_category));
        }
        else {
            if(!ParseUser.getCurrentUser().getUpdatedAt().toString().equals(application.getUserLastUpdate())){
                application.setUserLastUpdate(ParseUser.getCurrentUser().getUpdatedAt().toString());
                analysisData.clearAll();
            }
            else if(application.isIfAllergyUpdate()){
                analysisData.clearAll();
                application.setIfAllergyUpdate(false);
            }

            ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("ProductCategory");
            innerQuery.whereEqualTo("type", selected_category);
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Product");
            /* USTAWIONY LIMIT */
            query.setLimit(3000);
            query.orderByAscending("Name");
            query.whereMatchesQuery("IsIn", innerQuery);
            long startTime = System.currentTimeMillis();


            List<ParseObject> product_found = query.find();
            int number_of_threads = 160;
            ArrayList<Runnable> runnables = new ArrayList<>();
            ArrayList<Thread> threads = new ArrayList<>();
            ArrayList<ParseObject> objects;
            for (int i = 0; i < number_of_threads; i++) {

                if (i == number_of_threads - 1) {
                    objects = new ArrayList<ParseObject>(product_found.subList(i * product_found.size() / number_of_threads, product_found.size()));
                } else {
                    objects = new ArrayList<ParseObject>(product_found.subList(i * product_found.size() / number_of_threads, (i + 1) * product_found.size() / number_of_threads));
                }
                runnables.add(new AnalysisRunnable(products, objects, ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives,
                        ifParabens, ifPopularAllergens, ifAllAllergens, ifAnimalOrigin, ifDangerousForPregnant, skinType, hairType,avoidAllergens, selected_category));
                threads.add(new Thread(runnables.get(i), "Thread"));
                threads.get(i).start();
            }
            for (int i = 0; i < number_of_threads; i++) {
                threads.get(i).join();
            }
            analysisData.saveAnalysedProducts(selected_category,products);
        }




    }

    public void add_to_list(ArrayList<String> products){
        all_products = products;
        arrayAdapter = new SearchAdapter(this, 0, products){
            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View row;

                if (null == convertView) {
                    row = getLayoutInflater().inflate(android.R.layout.simple_list_item_1, null);
                } else {
                    row = convertView;
                }
                TextView tv = (TextView) row.findViewById(android.R.id.text1);
                tv.setText(Html.fromHtml((String) getItem(position)));
                return row;
            }
        };
        listView.setAdapter(arrayAdapter);
        ArrayList<String> all_products2 = new ArrayList<>();
        all_products2.addAll(all_products);
                Collections.sort(all_products2, new Comparator<String>() {
            public int compare(String s1, String s2) {
                // Skip all identical characters
                int len1 = s1.length();
                int len2 = s2.length();
                int i;
                char c1, c2;
                for (i = 0, c1 = 0, c2 = 0; (i < len1) && (i < len2) && (c1 = s1.charAt(i)) == (c2 = s2.charAt(i)); i++)
                    ;

                // Check end of string
                if (c1 == c2)
                    return (len1 - len2);

                // Check digit in first string
                if (Character.isDigit(c1)) {
                    // Check digit only in first string
                    if (!Character.isDigit(c2))
                        return (1);

                    // Scan all integer digits
                    int x1, x2;
                    for (x1 = i + 1; (x1 < len1) && Character.isDigit(s1.charAt(x1)); x1++)
                        ;
                    for (x2 = i + 1; (x2 < len2) && Character.isDigit(s2.charAt(x2)); x2++)
                        ;

                    // Longer integer wins, first digit otherwise
                    return (x2 == x1 ? c1 - c2 : x1 - x2);
                }

                // Check digit only in second string
                if (Character.isDigit(c2))
                    return (-1);

                // No digits
                return (c1 - c2);
            }
        });

        Collections.reverse(all_products2);
        System.out.println(all_products2);
        ArrayList<String> best_products = new ArrayList<String>(all_products2.subList(0, 30));
        System.out.println(best_products);

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String[] separated_term = s.toString().split(" ");
                StringBuilder term= new StringBuilder();
                for(int i=0; i<separated_term.length; i++){
                    if(!separated_term[i].equals("")){
                        term.append(separated_term[i]);
                        if(i<separated_term.length-1){
                            term.append(" ");
                        }
                    }
                }
                arrayAdapter.getFilter().filter(term.toString());
                arrayAdapter.notifyDataSetChanged();
                listView.setOnItemClickListener((parent, view, position, id) -> {
                    String wholeText = Html.fromHtml(arrayAdapter.getItem(position)).toString();
                    String[] separated = wholeText.split(" -- ");
                    System.out.println("Name: "+separated[1]);
                    System.out.println("Points: "+separated[0]);
                    Intent intent_product_details = new Intent(view.getContext(), ProductDetailsActivity.class);
                    intent_product_details.putExtra("ProductName", separated[1]);
                    intent_product_details.putExtra("ProductPoints", separated[0]);
                    intent_product_details.putExtra("ProductCategory", selected_category);
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST",(Serializable)best_products);
                    intent_product_details.putExtra("BUNDLE",args);
                    startActivity(intent_product_details);
                });
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        listView.setOnItemClickListener((parent, view, position, id) -> {
            String wholeText = Html.fromHtml(products.get(position)).toString();
            String[] separated = wholeText.split(" -- ");
            Intent intent_product_details = new Intent(view.getContext(), ProductDetailsActivity.class);
            intent_product_details.putExtra("ProductName", separated[1]);
            intent_product_details.putExtra("ProductPoints", separated[0]);
            intent_product_details.putExtra("ProductCategory", selected_category);
            Bundle args = new Bundle();
            args.putSerializable("ARRAYLIST",(Serializable)best_products);
            intent_product_details.putExtra("BUNDLE",args);
            startActivity(intent_product_details);
        });
    }

    public void getUserInformation(){
        hairType = ( (String) ParseUser.getCurrentUser().get("hairType"));
        skinType = ( (String) ParseUser.getCurrentUser().get("skinType"));

        ifAcne = (boolean) ParseUser.getCurrentUser().get("ifAcne");
        ifDermatitis = (boolean) ParseUser.getCurrentUser().get("ifDermatitis");
        ifDandruff = (boolean) ParseUser.getCurrentUser().get("ifDandruff");
        ifAlcohols = (boolean) ParseUser.getCurrentUser().get("ifAlcohols");
        ifSilicones = (boolean) ParseUser.getCurrentUser().get("ifSilicones");
        ifBenzylDerivatives = (boolean) ParseUser.getCurrentUser().get("ifBenzylDerivatives");
        ifParabens = (boolean) ParseUser.getCurrentUser().get("ifParabens");
        ifAnimalOrigin = (boolean) ParseUser.getCurrentUser().get("ifAnimalOrigin");
        ifPopularAllergens = (boolean) ParseUser.getCurrentUser().get("ifPopularAllergens");
        ifAllAllergens = (boolean) ParseUser.getCurrentUser().get("ifAllAllergens");
        ifDangerousForPregnant = (boolean) ParseUser.getCurrentUser().get("ifDangerousForPregnant");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("AvoidAllergen");
        query.selectKeys(Collections.singleton("AllergenName"));
        query.whereEqualTo("UserId",ParseUser.getCurrentUser().getObjectId());
        List<ParseObject> avoidAllergensObj = null;
        try {
            avoidAllergensObj = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert avoidAllergensObj != null;
        if(!avoidAllergensObj.isEmpty()){
            for(int i =0 ; i<avoidAllergensObj.size();i++){
                avoidAllergens.add(avoidAllergensObj.get(i).getString("AllergenName"));
            }
        }

    }

    public void showSortingPopup(View view){
        PopupMenu sortingPopup = new PopupMenu(this,view);
        sortingPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()){
                    case R.id.nameA_Z:
                        for(int i=0;i<products.size(); i++) {
                            String[] separated = products.get(i).split(" -- ");
                            products.set(i, separated[1] + " -- " + separated[0]);
                        }
                        Collections.sort(products,String.CASE_INSENSITIVE_ORDER);
                        for(int i=0;i<products.size(); i++) {
                            String[] separated = products.get(i).split(" -- ");
                            products.set(i, separated[1] + " -- " + separated[0]);
                        }
                        add_to_list(products);
                        return true;

                    case R.id.nameZ_A:
                        for(int i=0;i<products.size(); i++) {
                            String[] separated = products.get(i).split(" -- ");
                            products.set(i, separated[1] + " -- " + separated[0]);
                        }
                        Collections.sort(products,String.CASE_INSENSITIVE_ORDER);
                        Collections.reverse(products);
                        for(int i=0;i<products.size(); i++) {
                            String[] separated = products.get(i).split(" -- ");
                            products.set(i, separated[1] + " -- " + separated[0]);
                        }
                        add_to_list(products);
                        return true;
                    case R.id.fitting_from_worst:
                        Collections.sort(products, new Comparator<String>() {
                            public int compare(String s1, String s2) {
                                // Skip all identical characters
                                int len1 = s1.length();
                                int len2 = s2.length();
                                int i;
                                char c1, c2;
                                for (i = 0, c1 = 0, c2 = 0; (i < len1) && (i < len2) && (c1 = s1.charAt(i)) == (c2 = s2.charAt(i)); i++)
                                    ;

                                // Check end of string
                                if (c1 == c2)
                                    return (len1 - len2);

                                // Check digit in first string
                                if (Character.isDigit(c1)) {
                                    // Check digit only in first string
                                    if (!Character.isDigit(c2))
                                        return (1);

                                    // Scan all integer digits
                                    int x1, x2;
                                    for (x1 = i + 1; (x1 < len1) && Character.isDigit(s1.charAt(x1)); x1++)
                                        ;
                                    for (x2 = i + 1; (x2 < len2) && Character.isDigit(s2.charAt(x2)); x2++)
                                        ;

                                    // Longer integer wins, first digit otherwise
                                    return (x2 == x1 ? c1 - c2 : x1 - x2);
                                }

                                // Check digit only in second string
                                if (Character.isDigit(c2))
                                    return (-1);

                                // No digits
                                return (c1 - c2);
                            }
                        });
                        add_to_list(products);
                        return true;
                    case R.id.fitting_from_best:
                        Collections.sort(products, new Comparator<String>() {
                            public int compare(String s1, String s2) {
                                // Skip all identical characters
                                int len1 = s1.length();
                                int len2 = s2.length();
                                int i;
                                char c1, c2;
                                for (i = 0, c1 = 0, c2 = 0; (i < len1) && (i < len2) && (c1 = s1.charAt(i)) == (c2 = s2.charAt(i)); i++)
                                    ;

                                // Check end of string
                                if (c1 == c2)
                                    return (len1 - len2);

                                // Check digit in first string
                                if (Character.isDigit(c1)) {
                                    // Check digit only in first string
                                    if (!Character.isDigit(c2))
                                        return (1);

                                    // Scan all integer digits
                                    int x1, x2;
                                    for (x1 = i + 1; (x1 < len1) && Character.isDigit(s1.charAt(x1)); x1++)
                                        ;
                                    for (x2 = i + 1; (x2 < len2) && Character.isDigit(s2.charAt(x2)); x2++)
                                        ;

                                    // Longer integer wins, first digit otherwise
                                    return (x2 == x1 ? c1 - c2 : x1 - x2);
                                }

                                // Check digit only in second string
                                if (Character.isDigit(c2))
                                    return (-1);

                                // No digits
                                return (c1 - c2);
                            }
                        });
                        Collections.reverse(products);
                        add_to_list(products);
                        return true;
                    default:
                        return false;
                }
            }
        });
        sortingPopup.inflate(R.menu.sorting_popup);
        sortingPopup.show();


    }

    public void showInfoPopup(View view){
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

    }

    public static void slideView(View view,
                                 int currentHeight,
                                 int newHeight) {

        ValueAnimator slideAnimator = ValueAnimator
                .ofInt(currentHeight, newHeight)
                .setDuration(500);

        /* We use an update listener which listens to each tick
         * and manually updates the height of the view  */

        slideAnimator.addUpdateListener(animation1 -> {
            Integer value = (Integer) animation1.getAnimatedValue();
            view.getLayoutParams().height = value;
            view.requestLayout();
        });

        /*  We use an animationSet to play the animation  */

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.play(slideAnimator);
        animationSet.start();
    }

    public void load_products(){
        ProgressDialog dialog = new ProgressDialog(HomePageActivity.this);
        AsyncTask task = new AsyncTask<Void, Void, Void>() {
            protected void onPreExecute() {

                // TODO Auto-generated method stub
                super.onPreExecute();
                dialog.setMessage("Products loading...");
                dialog.show();
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            protected Void doInBackground(Void... params) {
                try {
                    search_product2(selected_category);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;

            }
            protected void onPostExecute(Void result) {
                dialog.dismiss();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        add_to_list(products);
                    }
                });
            }
        }.execute();
    }


}