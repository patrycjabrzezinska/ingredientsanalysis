package com.example.cosmeticspy;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static com.example.cosmeticspy.UserAccountActivity.setListViewHeightBasedOnItems;


public class SignupActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText editTextPersonName;
    EditText editTextPersonSurname;
    EditText editTextEmailAddress;
    EditText editTextUsername;
    EditText editTextPassword;
    EditText editTextConfirmPassword;
    boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives = false;
    boolean ifParabens, ifAnimalOrigin, ifPopularAllergens, ifAllAllergens, ifDangerousForPregnant = false;
    String gender = "other";
    String skinType, hairType = "normal";
    private Spinner spinner1, spinner2;
    private static final String[] hair_types = {"normal", "greasy", "dry"};
    private static final String[] skin_types = {"normal", "oily", "dry", "sensitive", "combination"};
    private PopupWindow POPUP_WINDOW_SCORE = null;
    ArrayList<String> allergens_list  = new ArrayList<>();
    ListView allergensListView;
    ArrayAdapter<String> arrayAdapter;

    ParseQuery<ParseObject> usersListQuery;
    ArrayList<ParseObject> allUsers = new ArrayList<ParseObject>();


    public static final String TAG = "ListViewExample";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        editTextPersonName = findViewById(R.id.editTextPersonName);
        editTextPersonSurname = findViewById(R.id.editTextPersonSurname);
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress);
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword);
        allergensListView = (ListView) findViewById(R.id.list_of_allergens);

        spinner1 = (Spinner)findViewById(R.id.spinner1);
        ArrayAdapter<String>adapter1 = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_spinner_item, hair_types);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        spinner1.setOnItemSelectedListener(new HairSpinnerClass());

        spinner2 = (Spinner)findViewById(R.id.spinner2);
        ArrayAdapter<String>adapter2 = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_spinner_item, skin_types);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(new SkinSpinnerClass());

        usersListQuery = ParseQuery.getQuery("_User");

        usersListQuery.setLimit(30000);
        usersListQuery.findInBackground((objects, e) -> {
            if (e == null) {
                for (int i = 0; i < objects.size(); i++) {
                    allUsers.add(objects.get(i));
                }
            } else {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });




    }


    public void singnup(View view) throws InterruptedException {
        ProgressDialog dialog = new ProgressDialog(SignupActivity.this);
        AsyncTask task = new AsyncTask<Void, Void, Void>() {
            protected void onPreExecute() {

                // TODO Auto-generated method stub
                super.onPreExecute();
                dialog.setMessage("Creating new account...");
                dialog.show();
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            protected Void doInBackground(Void... params) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            singnupAction();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return null;

            }
            protected void onPostExecute(Void result) {
                dialog.dismiss();
            }
        }.execute();
    }
    public void singnupAction() throws InterruptedException {
        boolean usernameCheck = false;
        for (int i = 0; i < allUsers.size(); i++) {
            System.out.println(allUsers.get(i).get("username").toString());//.get("username")
            System.out.println(editTextUsername.getText().toString());//.get("username")
            if(editTextUsername.getText().toString().equals(allUsers.get(i).get("username").toString())) {
                editTextUsername.setError("Username must be unique");
                usernameCheck = true;
            }
        }

        if(TextUtils.isEmpty(editTextEmailAddress.getText())) {
            editTextEmailAddress.setError("Email is required");
        } else if(TextUtils.isEmpty(editTextPassword.getText())) {
            editTextPassword.setError("Password is required");
        } else if(TextUtils.isEmpty(editTextConfirmPassword.getText())) {
            editTextConfirmPassword.setError("Confirm password");
        } else if(TextUtils.isEmpty(editTextPersonName.getText())) {
            editTextPersonName.setError("Name is required");
        } else if(TextUtils.isEmpty(editTextPersonSurname.getText())) {
            editTextPersonSurname.setError("Surname is required");
        } else if(TextUtils.isEmpty(editTextUsername.getText())) {
            editTextUsername.setError("Username is required");
        } else if(usernameCheck) {
        } else if(!editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) {
            Toast.makeText(SignupActivity.this, "Passwords must be the same", Toast.LENGTH_LONG).show();
        } else if(editTextPassword.getText().toString().length()<5) {
            Toast.makeText(SignupActivity.this, "Password must be at least 5 characters long", Toast.LENGTH_LONG).show();
        } else if(!containsDigit(editTextPassword.getText().toString())) {
            Toast.makeText(SignupActivity.this, "Password must contain at least one digit", Toast.LENGTH_LONG).show();
        } else {
            ParseUser user = new ParseUser();
            user.setUsername(editTextUsername.getText().toString());
            user.setPassword(editTextPassword.getText().toString());
            user.setEmail(editTextEmailAddress.getText().toString());
            user.put("name", editTextPersonName.getText().toString());
            user.put("surname", editTextPersonSurname.getText().toString());
            user.put("gender", gender);
            user.put("hairType", hairType);
            user.put("skinType", skinType);
            user.put("ifAcne", ifAcne);
            user.put("ifDermatitis", ifDermatitis);
            user.put("ifDandruff", ifDandruff);
            user.put("ifAlcohols", ifAlcohols);
            user.put("ifSilicones", ifSilicones);
            user.put("ifBenzylDerivatives", ifBenzylDerivatives);
            user.put("ifParabens", ifParabens);
            user.put("ifAnimalOrigin", ifAnimalOrigin);
            user.put("ifPopularAllergens", ifPopularAllergens);
            user.put("ifAllAllergens", ifAllAllergens);
            user.put("ifDangerousForPregnant", ifDangerousForPregnant);

            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if (e==null) {
                        Toast.makeText(SignupActivity.this, "Welcome!", Toast.LENGTH_LONG).show();
                    } else {
                        ParseUser.logOut();
                        Toast.makeText(SignupActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });




            ParseUser.logInInBackground(editTextEmailAddress.getText().toString(), editTextPassword.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if (user != null) {
                        Toast.makeText(SignupActivity.this, "Hello", Toast.LENGTH_LONG).show();

                    } else {
                        ParseUser.logOut();
                        Toast.makeText(SignupActivity.this, "Log in", Toast.LENGTH_LONG).show();
                    }
                }
            });

            Thread.sleep(4000);


            String id = user.getObjectId();

            for (String allergen1: allergens_list) {
                ParseObject entity = new ParseObject("AvoidAllergen");
                entity.put("AllergenName", allergen1);
                entity.put("UserId", id);
                entity.saveInBackground(e -> {
                    if (e==null){
                        Toast.makeText(SignupActivity.this, " ", Toast.LENGTH_LONG).show();
                    }else{
                        //Something went wrong
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            ParseUser.logOut();

            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }

    public void onCheckboxSpecificAllergenClicked(View view) {
        final App application = (App) getApplicationContext();
        boolean ifLoaded = application.getIfIngredientsLoaded();
        if (ifLoaded) {
            editTextPersonName = findViewById(R.id.editTextPersonName);
            String nameBefore = editTextPersonName.getText().toString();
            Intent intent_allergy_details = new Intent(view.getContext(), AllergenPopupLayoutActivity.class);
            intent_allergy_details.putExtras(intent_allergy_details.putExtra("mode", "signup"));

            startActivityForResult(intent_allergy_details, 1);

        }
        else {
            Toast.makeText(SignupActivity.this, "Wait - ingredients list is loading", Toast.LENGTH_LONG).show();
        }

    }

    public boolean containsDigit(String text) {
        boolean containsDigit = false;
        if (text != null && !text.isEmpty()) {
            for (char c : text.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }
        return containsDigit;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle bundle = data.getExtras();
        if(bundle != null){
            if(bundle.getString("result").equals("ok")) {
                allergens_list = bundle.getStringArrayList("list_of_allergens");
                arrayAdapter = new ArrayAdapter<>(SignupActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, allergens_list);
                allergensListView.setAdapter(arrayAdapter);
                setListViewHeightBasedOnItems(allergensListView);
            }
        }
    }

    class HairSpinnerClass implements AdapterView.OnItemSelectedListener
    {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id)
        {
            switch (position) {
                case 0:
                    hairType = "normal";
                    break;
                case 1:
                    hairType = "greasy";
                    break;
                case 2:
                    hairType = "dry";
                    break;        }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            hairType = "normal";

        }
    }

    class SkinSpinnerClass implements AdapterView.OnItemSelectedListener
    {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id)
        {
            switch (position) {
                case 0:
                    skinType = "normal";
                    break;
                case 1:
                    skinType = "oily";
                    break;
                case 2:
                    skinType = "dry";
                    break;
                case 3:
                    skinType = "sensitive";
                    break;
                case 4:
                    skinType = "combination";
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            skinType = "normal";

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
        setContentView(R.layout.activity_signup);
        Spinner spinner1 = (Spinner)parent;
        Spinner spinner2 = (Spinner)parent;
        if(spinner1.getId() == R.id.spinner1)

        {
            switch (position) {
                case 0:
                    hairType = "normal";
                    break;
                case 1:
                    hairType = "greasy";
                    break;
                case 2:
                    hairType = "dry";
                    break;        }
            if(spinner2.getId() == R.id.spinner2)
            {
                switch (position) {
                    case 0:
                        skinType = "normal";
                        break;
                    case 1:
                        skinType = "oily";
                        break;
                    case 2:
                        skinType = "dry";
                        break;
                    case 3:
                        skinType = "sensitive";
                        break;
                    case 4:
                        skinType = "combination";
                        break;
                }
            }


        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub
    }

    public void onCheckboxIHaveAcneClicked(View view) {
        ifAcne = true;
    }

    public void onCheckboxIHaveDermatitisClicked(View view) { ifDermatitis = true; }

    public void onCheckboxIHaveDandruffClicked(View view) { ifDandruff = true; }

    public void onCheckboxIAvoidAlcoholsClicked(View view) { ifAlcohols = true; }

    public void onCheckboxIAvoidSiliconesClicked(View view) { ifSilicones = true; }

    public void onCheckboxIAvoidBenzylDerivativesClicked(View view) { ifBenzylDerivatives = true; }

    public void onCheckboxIAvoidParabensClicked(View view) { ifParabens = true; }

    public void onCheckboxIAvoidPopularAllergensClicked(View view) { ifPopularAllergens = true; }

    public void onCheckboxIAvoidAllAllergensClicked(View view) { ifAllAllergens = true; }

    public void onCheckboxIAvoidAnimalOriginClicked(View view) { ifAnimalOrigin = true; }

    public void onCheckboxIAvoidDangerousForPregnantClicked(View view) { ifDangerousForPregnant = true; }

    public void female(View view) {
        view.setSelected(true);
        ImageView male= (ImageView) findViewById(R.id.male);
        ImageView female= (ImageView) findViewById(R.id.female);
        ImageView other= (ImageView) findViewById(R.id.other);

        male.setBackgroundResource(R.drawable.male);
        female.setBackgroundResource(R.drawable.female_pressed);
        other.setBackgroundResource(R.drawable.other);
        gender = "female";
    }

    public void other(View view) {
        view.setSelected(true);
        ImageView male= (ImageView) findViewById(R.id.male);
        ImageView female= (ImageView) findViewById(R.id.female);
        ImageView other= (ImageView) findViewById(R.id.other);

        male.setBackgroundResource(R.drawable.male);
        female.setBackgroundResource(R.drawable.female);
        other.setBackgroundResource(R.drawable.other_pressed);
        gender = "other";
    }

    public void male(View view) {
        view.setSelected(true);
        ImageView male= (ImageView) findViewById(R.id.male);
        ImageView female= (ImageView) findViewById(R.id.female);
        ImageView other= (ImageView) findViewById(R.id.other);

        male.setBackgroundResource(R.drawable.male_pressed);
        female.setBackgroundResource(R.drawable.female);
        other.setBackgroundResource(R.drawable.other);
        gender = "male";
    }
}