package com.example.cosmeticspy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class EditUserActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText editTextPersonName;
    EditText editTextPersonSurname;
    EditText editTextEmailAddress;
    EditText editTextPassword;
    EditText editTextConfirmPassword;
    CheckBox AcneCheckbox, DermatitisCheckbox, DandruffCheckbox, AlcoholsCheckbox;
    CheckBox SiliconesCheckbox, BenzylDerivativesCheckbox, ParabensCheckbox, AnimalOriginCheckbox;
    CheckBox PopularAllergensCheckbox, AllAllergensCheckBox, DangerousForPregnantCheckbox;
    boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives = false;
    boolean ifParabens, ifAnimalOrigin, ifPopularAllergens, ifAllAllergens, ifDangerousForPregnant = false;
    String skinType, hairType = "normal";
    private Spinner spinner1, spinner2;
    private static final String[] hair_types = {"normal", "greasy", "dry"};
    private static final String[] skin_types = {"normal", "oily", "dry", "sensitive", "combination"};
    String name, surname, email, gender, genderBeforeEdit, hairTypeBeforeEdit, skinTypeBeforeEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        editTextPersonName = findViewById(R.id.editTextPersonName);
        editTextPersonSurname = findViewById(R.id.editTextPersonSurname);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword);
        AcneCheckbox = findViewById(R.id.checkbox_acneprone);
        DermatitisCheckbox = findViewById(R.id.checkbox_atopicdermatitis);
        DandruffCheckbox = findViewById(R.id.checkbox_dandruff);
        AlcoholsCheckbox = findViewById(R.id.checkbox_alcohols);
        SiliconesCheckbox = findViewById(R.id.checkbox_silicones);
        BenzylDerivativesCheckbox = findViewById(R.id.checkbox_benzyl_derivatives);
        ParabensCheckbox = findViewById(R.id.checkbox_parabens);
        AnimalOriginCheckbox = findViewById(R.id.checkbox_animal_origin);
        PopularAllergensCheckbox = findViewById(R.id.checkbox_popular_allergens);
        AllAllergensCheckBox = findViewById(R.id.checkbox_all_allergens);
        DangerousForPregnantCheckbox = findViewById(R.id.checkbox_dangerous_for_pregnant);

        spinner1 = findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(EditUserActivity.this, android.R.layout.simple_spinner_item, hair_types);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        spinner1.setOnItemSelectedListener(new HairSpinnerClass());

        spinner2 = findViewById(R.id.spinner2);
        ArrayAdapter<String>adapter2 = new ArrayAdapter<String>(EditUserActivity.this, android.R.layout.simple_spinner_item, skin_types);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(new SkinSpinnerClass());

        ParseUser user = ParseUser.getCurrentUser();
        name = (String) user.get("name");
        editTextPersonName.setText(name);
        surname = (String) user.get("surname");
        editTextPersonSurname.setText(surname);

        gender = (String) user.get("gender");
        genderBeforeEdit = (String) user.get("gender");

        hairType = (String) user.get("hairType");
        hairTypeBeforeEdit = (String) user.get("hairType");
        if (hairType.equals("normal")) { spinner1.setSelection(0);}
        else if (hairType.equals("greasy")) { spinner1.setSelection(1);}
        else if (hairType.equals("dry")) { spinner1.setSelection(2);}

        skinType = (String) user.get("skinType");
        skinTypeBeforeEdit = (String) user.get("skinType");
        if (skinType.equals("normal")) { spinner2.setSelection(0);}
        else if (skinType.equals("oily")) { spinner2.setSelection(1);}
        else if (skinType.equals("dry")) { spinner2.setSelection(2);}
        else if (skinType.equals("sensitive")) { spinner2.setSelection(3);}
        else if (skinType.equals("combination")) { spinner2.setSelection(4);}

        ifAcne = (Boolean) user.get("ifAcne");
        AcneCheckbox.setChecked(ifAcne);
        ifDermatitis = (Boolean) user.get("ifDermatitis");
        DermatitisCheckbox.setChecked(ifDermatitis);
        ifDandruff = (Boolean) user.get("ifDandruff");
        DandruffCheckbox.setChecked(ifDandruff);

        ifAlcohols = (Boolean) user.get("ifAlcohols");
        AlcoholsCheckbox.setChecked(ifAlcohols);
        ifSilicones = (Boolean) user.get("ifSilicones");
        SiliconesCheckbox.setChecked(ifSilicones);
        ifBenzylDerivatives = (Boolean) user.get("ifBenzylDerivatives");
        BenzylDerivativesCheckbox.setChecked(ifBenzylDerivatives);
        ifParabens = (Boolean) user.get("ifParabens");
        ParabensCheckbox.setChecked(ifParabens);
        ifAnimalOrigin = (Boolean) user.get("ifAnimalOrigin");
        AnimalOriginCheckbox.setChecked(ifAnimalOrigin);
        ifPopularAllergens = (Boolean) user.get("ifPopularAllergens");
        PopularAllergensCheckbox.setChecked(ifPopularAllergens);
        ifAllAllergens = (Boolean) user.get("ifAllAllergens");
        AllAllergensCheckBox.setChecked(ifAllAllergens);
        ifDangerousForPregnant = (Boolean) user.get("ifDangerousForPregnant");
        DangerousForPregnantCheckbox.setChecked(ifDangerousForPregnant);

        ImageView male= (ImageView) findViewById(R.id.male);
        ImageView female= (ImageView) findViewById(R.id.female);
        ImageView other= (ImageView) findViewById(R.id.other);
        switch(gender) {
            case "female":
                male.setBackgroundResource(R.drawable.male);
                female.setBackgroundResource(R.drawable.female_pressed);
                other.setBackgroundResource(R.drawable.other);
                break;
            case "male":
                male.setBackgroundResource(R.drawable.male_pressed);
                female.setBackgroundResource(R.drawable.female);
                other.setBackgroundResource(R.drawable.other);
                break;
            case "other":
                male.setBackgroundResource(R.drawable.male);
                female.setBackgroundResource(R.drawable.female);
                other.setBackgroundResource(R.drawable.other_pressed);
                break;
        }
    }

    public void edit(View view) throws InterruptedException  {
        ParseUser user = ParseUser.getCurrentUser();

        if(TextUtils.isEmpty(editTextPersonName.getText())) {
            editTextPersonName.setError("Name is required");
        } else if(TextUtils.isEmpty(editTextPersonSurname.getText())) {
            editTextPersonSurname.setError("Surname is required");
        }
        else {
            if(!editTextPersonName.getText().toString().equals( (String) user.get("name"))) {
                user.put("name", editTextPersonName.getText().toString());
            }
            if(!editTextPersonSurname.getText().toString().equals( (String) user.get("surname"))) {
                user.put("surname", editTextPersonSurname.getText().toString());
            }
            if(!gender.equals(genderBeforeEdit)) { user.put("gender", gender); }
            if(!hairType.equals(hairTypeBeforeEdit)) { user.put("hairType", hairType); }
            if(!skinType.equals(skinTypeBeforeEdit)) { user.put("skinType", skinType); }
            if(AcneCheckbox.isChecked()) { user.put("ifAcne", true); }
            else { user.put("ifAcne", false); }
            if(DermatitisCheckbox.isChecked()) { user.put("ifDermatitis", true); }
            else { user.put("ifDermatitis", false); }
            if(DandruffCheckbox.isChecked()) { user.put("ifDandruff", true); }
            else { user.put("ifDandruff", false); }

            if(AlcoholsCheckbox.isChecked()) { user.put("ifAlcohols", true); }
            else { user.put("ifAlcohols", false); }
            if(SiliconesCheckbox.isChecked()) { user.put("ifSilicones", true); }
            else { user.put("ifSilicones", false); }
            if(BenzylDerivativesCheckbox.isChecked()) { user.put("ifBenzylDerivatives", true); }
            else { user.put("ifBenzylDerivatives", false); }
            if(ParabensCheckbox.isChecked()) { user.put("ifParabens", true); }
            else { user.put("ifParabens", false); }
            if(AnimalOriginCheckbox.isChecked()) { user.put("ifAnimalOrigin", true); }
            else { user.put("ifAnimalOrigin", false); }
            if(PopularAllergensCheckbox.isChecked()) { user.put("ifPopularAllergens", true); }
            else { user.put("ifPopularAllergens", false); }
            if(AllAllergensCheckBox.isChecked()) { user.put("ifAllAllergens", true); }
            else { user.put("ifAllAllergens", false); }
            if(DangerousForPregnantCheckbox.isChecked()) { user.put("ifDangerousForPregnant", true); }
            else { user.put("ifDangerousForPregnant", false); }

            user.saveInBackground(e -> {
                if(e==null){
                    Toast.makeText(this, "Save Successful", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            Intent intent = new Intent(EditUserActivity.this, UserAccountActivity.class);
            startActivity(intent);

        }

    }


    class HairSpinnerClass implements AdapterView.OnItemSelectedListener
    {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id)
        {
            switch (position) {
                case 0:
                    hairType = "normal";
                    break;
                case 1:
                    hairType = "greasy";
                    break;
                case 2:
                    hairType = "dry";
                    break;        }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            ParseUser user = ParseUser.getCurrentUser();
            hairType = (String) user.get("hairType");

        }
    }

    class SkinSpinnerClass implements AdapterView.OnItemSelectedListener
    {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id)
        {
            switch (position) {
                case 0:
                    skinType = "normal";
                    break;
                case 1:
                    skinType = "oily";
                    break;
                case 2:
                    skinType = "dry";
                    break;
                case 3:
                    skinType = "sensitive";
                    break;
                case 4:
                    skinType = "combination";
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            ParseUser user = ParseUser.getCurrentUser();
            skinType = (String) user.get("skinType");

        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        Spinner spinner1 = (Spinner)parent;
        Spinner spinner2 = (Spinner)parent;
        if(spinner1.getId() == R.id.spinner1)

        {
            switch (position) {
                case 0:
                    hairType = "normal";
                    break;
                case 1:
                    hairType = "greasy";
                    break;
                case 2:
                    hairType = "dry";
                    break;        }
            if(spinner2.getId() == R.id.spinner2)
            {
                switch (position) {
                    case 0:
                        skinType = "normal";
                        break;
                    case 1:
                        skinType = "oily";
                        break;
                    case 2:
                        skinType = "dry";
                        break;
                    case 3:
                        skinType = "sensitive";
                        break;
                    case 4:
                        skinType = "combination";
                        break;
                }
            }


        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub
    }

    public void onCheckboxIHaveAcneClicked(View view) {
        ifAcne = true;
    }

    public void onCheckboxIHaveDermatitisClicked(View view) { ifDermatitis = true; }

    public void onCheckboxIHaveDandruffClicked(View view) { ifDandruff = true; }

    public void onCheckboxIAvoidAlcoholsClicked(View view) { ifAlcohols = true; }

    public void onCheckboxIAvoidSiliconesClicked(View view) { ifSilicones = true; }

    public void onCheckboxIAvoidBenzylDerivativesClicked(View view) { ifBenzylDerivatives = true; }

    public void onCheckboxIAvoidParabensClicked(View view) { ifParabens = true; }

    public void onCheckboxIAvoidPopularAllergensClicked(View view) { ifPopularAllergens = true; }

    public void onCheckboxIAvoidAllAllergensClicked(View view) { ifAllAllergens = true; }

    public void onCheckboxIAvoidAnimalOriginClicked(View view) { ifAnimalOrigin = true; }

    public void onCheckboxIAvoidDangerousForPregnantClicked(View view) { ifDangerousForPregnant = true; }

    public void female(View view) {
        view.setSelected(true);
        ImageView male= (ImageView) findViewById(R.id.male);
        ImageView female= (ImageView) findViewById(R.id.female);
        ImageView other= (ImageView) findViewById(R.id.other);

        male.setBackgroundResource(R.drawable.male);
        female.setBackgroundResource(R.drawable.female_pressed);
        other.setBackgroundResource(R.drawable.other);
        gender = "female";
    }

    public void other(View view) {
        view.setSelected(true);
        ImageView male= (ImageView) findViewById(R.id.male);
        ImageView female= (ImageView) findViewById(R.id.female);
        ImageView other= (ImageView) findViewById(R.id.other);

        male.setBackgroundResource(R.drawable.male);
        female.setBackgroundResource(R.drawable.female);
        other.setBackgroundResource(R.drawable.other_pressed);
        gender = "other";
    }

    public void male(View view) {
        view.setSelected(true);
        ImageView male= (ImageView) findViewById(R.id.male);
        ImageView female= (ImageView) findViewById(R.id.female);
        ImageView other= (ImageView) findViewById(R.id.other);

        male.setBackgroundResource(R.drawable.male_pressed);
        female.setBackgroundResource(R.drawable.female);
        other.setBackgroundResource(R.drawable.other);
        gender = "male";
    }



}