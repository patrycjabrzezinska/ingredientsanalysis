package com.example.cosmeticspy;

import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import auxiliaryClasses.Ingredient;

public class ProductDetailsActivity extends AppCompatActivity {
    TextView productName;
    TextView productCategory;
    TextView productPoints;
    Button buttonAnalysis;
    Button buttonSubstitutes;
    ArrayList<Ingredient> ingredients = new ArrayList<>();
    ArrayList<String> ingredientsNames = new ArrayList<>();
    String product_categories = "";
    String product_points;
    ArrayList<String> categories;
    String product_ingredient;
    HashMap<String, List<String>> listIngredientDetails = new HashMap<>();
    boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives, ifParabens, ifPopularAllergens, ifAllAllergens, ifAnimalOrigin, ifDangerousForPregnant;
    String skinType, hairType;
    ArrayList<String> avoidAllergens = new ArrayList<>();
    String product_name, cat;
    int numberOfGoodIng = 0;
    int numberOfBadIng = 0;
    int numberOfNeutralIng = 0;
    boolean ifAllergy = false;
    ArrayList<String> init_ingredients = new ArrayList<>();
    ArrayList<Product> products_with_rate = new ArrayList<>();
    ArrayList<String> products_with_rate2 = new ArrayList<>();
    ArrayList<String> sub = new ArrayList<>();
    ArrayList<String> object = new ArrayList<>();
    LinearLayout linlaHeaderProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        productName = (TextView) findViewById(R.id.product_name);
        productCategory = (TextView) findViewById(R.id.product_category);
        productPoints = (TextView) findViewById(R.id.product_points);
        buttonAnalysis = (Button) findViewById(R.id.button_analysis);
        buttonSubstitutes = (Button) findViewById(R.id.button_substitutes);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        bottomNavigationView.getMenu().getItem(1).setChecked(true);
        buttonAnalysis.setBackgroundColor(ContextCompat.getColor(ProductDetailsActivity.this, R.color.blue_pressed));
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            product_name = bundle.getString("ProductName");
            productName.setText(product_name);
            System.out.println(product_name);
            cat = bundle.getString("ProductCategory");
            productCategory.setText(cat);
            System.out.println(cat);
            product_points = bundle.getString("ProductPoints");
            productPoints.setText(product_points);
            System.out.println(product_points);
            prepareAnalysis();
        }

        buttonAnalysis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAnalysis.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.blue_pressed));
                buttonSubstitutes.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.our_blue));
                replaceFragmentIngredients(product_name);

            }
        });
        buttonSubstitutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAnalysis.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.our_blue));
                buttonSubstitutes.setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.blue_pressed));
                try {
//                    substitutes();
                    replaceFragmentSubstitutes();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_search:
                        Intent intent_search = new Intent(ProductDetailsActivity.this, HomePageActivity.class);
                        startActivity(intent_search);
                        break;
                    case R.id.action_camera:
                        Intent intent_camera = new Intent(ProductDetailsActivity.this, CameraInstructionsActivity.class);
                        startActivity(intent_camera);
                        break;
                    case R.id.action_account:
                        Intent intent_account = new Intent(ProductDetailsActivity.this, UserAccountActivity.class);
                        startActivity(intent_account);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.onBackPressed();
    }


    private void replaceFragmentSubstitutes() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        NormalListFragment frag = new NormalListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("Category", cat);
        bundle.putSerializable("ProductsWithRate2", (Serializable) object);
        bundle.putSerializable("Substitutes", sub);
        frag.setArguments(bundle);
        transaction.replace(R.id.fl_product_details, frag);
        transaction.commit();
    }

    private void replaceFragmentIngredients(String product_name) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ExpandableListFragment frag2 = new ExpandableListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ProductName", product_name);
        bundle.putSerializable("IngredientsNames", ingredientsNames);
        bundle.putSerializable("ListIngredientDetails", listIngredientDetails);
        frag2.setArguments(bundle);
        transaction.replace(R.id.fl_product_details, frag2);
        transaction.commit();
    }

    public void prepareIngredientsList(String product_name) {
        ParseQuery<ParseObject> query_product = ParseQuery.getQuery("Product");
        query_product.whereEqualTo("Name", product_name);
        query_product.include("ProductCategory");
        query_product.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(ParseObject product, ParseException e) {
                if (e == null) {
                    ParseRelation<ParseObject> category_relation = product.getRelation("IsIn");
                    ParseQuery<ParseObject> query_category = category_relation.getQuery();
                    query_category.findInBackground((obj_categories, e1) -> {
                        if (e1 == null) {
                            for (int i = 0; i < obj_categories.size(); i++) {
                                if (i == obj_categories.size() - 1) {
                                    product_categories = product_categories + obj_categories.get(i).getString("type");
                                } else {
                                    product_categories = product_categories + obj_categories.get(i).getString("type") + ", ";
                                }
                            }
                        }
                    });
                    ParseRelation<ParseObject> ingredient_relation = product.getRelation("ConsistsOf");
                    ParseQuery<ParseObject> ingredient_query = ingredient_relation.getQuery();
                    ingredient_query.orderByAscending("INCIName");
                    ingredient_query.findInBackground((obj_ingredients, e1) -> {
                        if (e1 == null) {
                            System.out.println("INITIAL SIZE");
                            System.out.println(obj_ingredients.size());
                            for (int i = 0; i < obj_ingredients.size(); i++) {
                                product_ingredient = obj_ingredients.get(i).getString("INCIname");
                                ingredients.add(new Ingredient(obj_ingredients.get(i).getString("INCIname"),
                                        obj_ingredients.get(i).getInt("ifParaben"),
                                        obj_ingredients.get(i).getInt("ifAllergen"),
                                        obj_ingredients.get(i).getInt("ifPopularAllergen"),
                                        obj_ingredients.get(i).getInt("ifAnimalOrigin"),
                                        obj_ingredients.get(i).getInt("ifBenzylDerivative"),
                                        obj_ingredients.get(i).getInt("ifAlcohol"),
                                        obj_ingredients.get(i).getInt("ifSilicone"),
                                        obj_ingredients.get(i).getInt("ifDangerousForPregnant"),
                                        obj_ingredients.get(i).getInt("influenceOnOilySkin"),
                                        obj_ingredients.get(i).getInt("influenceOnDrySkin"),
                                        obj_ingredients.get(i).getInt("influenceOnCombinationSkin"),
                                        obj_ingredients.get(i).getInt("influenceOnAcneProne"),
                                        obj_ingredients.get(i).getInt("influenceOnSensitiveSkin"),
                                        obj_ingredients.get(i).getInt("influenceOnDryHair"),
                                        obj_ingredients.get(i).getInt("influenceOnOilyHair"),
                                        obj_ingredients.get(i).getInt("influenceOnDandruff"),
                                        obj_ingredients.get(i).getInt("influenceOnAtopicDermatitis")));


                                boolean ifAllergyToIngredient = false;
                                if (product_categories.contains("Hair dye") || product_categories.contains("Hair spray")
                                        || product_categories.contains("Hair") || product_categories.contains("Shampoo")
                                        || product_categories.contains("Conditioner")) {
                                    ingredients.get(i).countHairType(hairType);
                                    if (ifDandruff) {
                                        ingredients.get(i).countIfDandruff();
                                    }
                                }
                                if (product_categories.contains("Facial skincare") || product_categories.contains("Body skincare")
                                        || product_categories.contains("Anti aging") || product_categories.contains("Foot cream&lotion")
                                        || product_categories.contains("Lip balm&cream") || product_categories.contains("Skincare")
                                        || product_categories.contains("Body moisturiser") || product_categories.contains("Eye cream")
                                        || product_categories.contains("Suncream") || product_categories.contains("Hand cream&lotion")
                                        || product_categories.contains("Face moisturiser") || product_categories.contains("Shower gel")) {
                                    ingredients.get(i).countSkinType(skinType);
                                    if (ifAcne) {
                                        ingredients.get(i).countIfAcne();
                                    }
                                    if (ifDermatitis) {
                                        ingredients.get(i).countIfDermatitis();
                                    }
                                }

                                if (ifAlcohols) {
                                    ingredients.get(i).countIfAlcohol();
                                }

                                if (ifSilicones) {
                                    ingredients.get(i).countIfSilicone();
                                }
                                if (ifBenzylDerivatives) {
                                    ingredients.get(i).countIfBenzylDerivative();
                                }

                                if (ifParabens) {
                                    ingredients.get(i).countIfParaben();
                                }

                                if (ifAnimalOrigin) {
                                    ingredients.get(i).countIfAnimalOrigin();
                                }

                                if (ifPopularAllergens) {
                                    ingredients.get(i).countIfPopularAllergen();
                                }

                                if (ifAllAllergens) {
                                    ingredients.get(i).countIfAllAllergen();
                                }
                                if (ifDangerousForPregnant) {
                                    ingredients.get(i).countIfDangerousForPregnant();
                                }
                                ingredients.get(i).countIngredientAverage();
                                ifAllergyToIngredient = ingredients.get(i).checkIfUserAllergen(avoidAllergens);
                                ifAllergy = ifAllergyToIngredient;
                                String description = "";
                                if (ifAllergyToIngredient) {
                                    description = new String(Character.toChars(0x1F61E)) + new String(Character.toChars(0x2757));
                                    ingredients.get(i).getIngredientInfo().add("You have allergy to this ingredient !");
                                } else if (ingredients.get(i).getPoints() < 0.9) {

                                    description = new String(Character.toChars(0x1F620));
                                    numberOfBadIng++;
                                } else if (ingredients.get(i).getPoints() > 1.1) {
                                    description = new String(Character.toChars(0x1F970));
                                    numberOfGoodIng++;

                                } else {
                                    description = new String(Character.toChars(0x1F636));
                                    ingredients.get(i).getIngredientInfo().add("This ingredient doesn't have negative, nor positive influence on your body");
                                    numberOfNeutralIng++;
                                }

                                ingredientsNames.add(description + " " + product_ingredient);
                                listIngredientDetails.put(ingredientsNames.get(i), ingredients.get(i).getIngredientInfo());

                            }
                        }

                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        ExpandableListFragment frag2 = new ExpandableListFragment();
                        Bundle bundle2 = new Bundle();
                        bundle2.putString("ProductName", product_name);
                        bundle2.putSerializable("IngredientsNames", ingredientsNames);
                        bundle2.putSerializable("ListIngredientDetails", listIngredientDetails);
                        frag2.setArguments(bundle2);
                        transaction.replace(R.id.fl_product_details, frag2);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    });
                }
            }
        });
    }

    public void getUserInformation() {
        hairType = ((String) ParseUser.getCurrentUser().get("hairType"));
        skinType = ((String) ParseUser.getCurrentUser().get("skinType"));

        ifAcne = (boolean) ParseUser.getCurrentUser().get("ifAcne");
        ifDermatitis = (boolean) ParseUser.getCurrentUser().get("ifDermatitis");
        ifDandruff = (boolean) ParseUser.getCurrentUser().get("ifDandruff");
        ifAlcohols = (boolean) ParseUser.getCurrentUser().get("ifAlcohols");
        ifSilicones = (boolean) ParseUser.getCurrentUser().get("ifSilicones");
        ifBenzylDerivatives = (boolean) ParseUser.getCurrentUser().get("ifBenzylDerivatives");
        ifParabens = (boolean) ParseUser.getCurrentUser().get("ifParabens");
        ifAnimalOrigin = (boolean) ParseUser.getCurrentUser().get("ifAnimalOrigin");
        ifPopularAllergens = (boolean) ParseUser.getCurrentUser().get("ifPopularAllergens");
        ifAllAllergens = (boolean) ParseUser.getCurrentUser().get("ifAllAllergens");
        ifDangerousForPregnant = (boolean) ParseUser.getCurrentUser().get("ifDangerousForPregnant");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("AvoidAllergen");
        query.selectKeys(Collections.singleton("AllergenName"));
        query.whereEqualTo("UserId", ParseUser.getCurrentUser().getObjectId());
        List<ParseObject> avoidAllergensObj = null;
        try {
            avoidAllergensObj = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert avoidAllergensObj != null;
        if (!avoidAllergensObj.isEmpty()) {
            for (int i = 0; i < avoidAllergensObj.size(); i++) {
                avoidAllergens.add(avoidAllergensObj.get(i).getString("AllergenName"));
            }
        }

    }

    public void substitutes3(String product_name, ArrayList<Product> products_with_rate) throws Throwable {
        //Finding first product ingredients
        ParseQuery<ParseObject> query_product1 = ParseQuery.getQuery("Product");
        query_product1.include("ProductCategory");
        query_product1.whereEqualTo("Name", product_name);
        List<ParseObject> product1 = query_product1.find();
        for (int j = 0; j < product1.size(); j++) {
            ParseRelation<ParseObject> ingredient_relation1 = product1.get(j).getRelation("ConsistsOf");
            ParseQuery<ParseObject> ingredient_query1 = ingredient_relation1.getQuery();
            ingredient_query1.orderByAscending("INCIName");
            List<ParseObject> obj_ingredients = ingredient_query1.find();
            for (int i = 0; i < obj_ingredients.size(); i++) {
                init_ingredients.add(obj_ingredients.get(i).getString("INCIname"));
            }
        }

        int number_of_threads = 30;
        ArrayList<Runnable> runnables = new ArrayList<>();
        ArrayList<Thread> threads = new ArrayList<>();
        ArrayList<Product> objects;

        for (int i = 0; i < number_of_threads; i++) {
            if (i == number_of_threads - 1) {
                objects = new ArrayList<Product>(products_with_rate.subList(i * products_with_rate.size() / number_of_threads, products_with_rate.size()));
            } else {
                objects = new ArrayList<Product>(products_with_rate.subList(i * products_with_rate.size() / number_of_threads, (i + 1) * products_with_rate.size() / number_of_threads));
            }
            runnables.add(new SubstitutesRunnable(sub, objects, init_ingredients));
            System.out.println("--------------------------------" + i + "-------------------------------------------");
            threads.add(new Thread(runnables.get(i), "Thread"));
            threads.get(i).start();
        }
        for (int i = 0; i < number_of_threads; i++) {
            threads.get(i).join();
        }

        Collections.sort(products_with_rate);
        for (Product s : products_with_rate) {
            System.out.println(s.getSim() + "   " + s.getProductName() + "   " + s.getScore());
        }
        int k= 0;

        int points_to_compare = Integer.parseInt(product_points.replace("%", ""));
        int num_of_similar = 0;
        while (num_of_similar < 10) {
            if (!products_with_rate.get(k).getProductName().equals(product_name)) {
                if (products_with_rate.get(k).getScore()>= points_to_compare ){
                    sub.add(products_with_rate.get(k).getScore() + "% -- " + products_with_rate.get(k).getProductName());
                }
                num_of_similar++;
            }
            k++;
        }
        if(sub.size()==0){
            k= 0;
            while (sub.size() < 3) {
                if (!products_with_rate.get(k).getProductName().equals(product_name)) {
                    sub.add(products_with_rate.get(k).getScore() + "% -- " + products_with_rate.get(k).getProductName());
                }
                k++;
            }
        }

        Collections.sort(sub);
        Collections.reverse(sub);

    }

    public void prepareAnalysis() {
        new AsyncTask<Void, Void, Void>() {
            protected void onPreExecute() {
                super.onPreExecute();
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }



            @RequiresApi(api = Build.VERSION_CODES.N)
            protected Void doInBackground(Void... params) {
                getUserInformation();
                prepareIngredientsList(product_name);

                Intent intent = getIntent();
                Bundle args = intent.getBundleExtra("BUNDLE");
                object = (ArrayList<String>) args.getSerializable("ARRAYLIST");
                if (products_with_rate.size() > 0) {
                    products_with_rate.clear();
                }
                for (int k = 0; k < object.size(); k++) {
                    String[] separated = object.get(k).split(" -- ");
                    separated[0] = separated[0].substring(3, separated[0].length() - 5);
                    products_with_rate2.add(separated[1]);
                    products_with_rate.add(new Product(separated[1], Integer.parseInt(separated[0]), 0));
                }

                try {
                    substitutes3(product_name, products_with_rate);
                } catch (
                        Throwable throwable) {
                    throwable.printStackTrace();
                }
                return null;

            }

            protected void onPostExecute(Void result) {
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        }.execute();

    }


}