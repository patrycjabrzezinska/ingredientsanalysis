package com.example.cosmeticspy;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends AppCompatActivity {

    EditText editTextEmailAddress, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress);
        editTextPassword = findViewById(R.id.editTextPassword);
    }

    public void login(View view) {
        if(TextUtils.isEmpty(editTextEmailAddress.getText())) {
            editTextEmailAddress.setError("Email is required");
        } else if(TextUtils.isEmpty(editTextPassword.getText())) {
            editTextPassword.setError("Password is required");
        } else {
            ParseUser.logInInBackground(editTextEmailAddress.getText().toString(), editTextPassword.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    if (parseUser != null) {
                        String message = "Welcome back " + parseUser.getUsername().toString() +"!";
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                        final App application = (App) getApplicationContext();
                        application.setUserLastUpdate(ParseUser.getCurrentUser().getUpdatedAt().toString());
                        Intent intent = new Intent(LoginActivity.this, HomePageActivity.class);
                        startActivity(intent);
                    } else {
                        ParseUser.logOut();
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

    }

    public void signup(View view) {
        Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
        startActivity(intent);
    }
}