package com.example.cosmeticspy;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class IngredientDetailsActivity extends AppCompatActivity {
    TextView ingredientName;
    TextView ingredientDetails;
    TextView functionDescription;
    ListView listIngredientDetails;
    String details = "Function: ";
    String function;
    boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives, ifParabens, ifPopularAllergens, ifAllAllergens, ifAnimalOrigin, ifDangerousForPregnant;
    String skinType, hairType;
    List<String> ingredient_details = new ArrayList<>();
    String negative = new String(Character.toChars(0x1F610));
    String positive = new String(Character.toChars(0x263A));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredient_details);
        ingredientName = (TextView) findViewById(R.id.ingredient_name);
        ingredientDetails = (TextView) findViewById(R.id.ingredient_details);
        listIngredientDetails = (ListView) findViewById(R.id.list_ingredient_details);
        functionDescription = (TextView) findViewById(R.id.function_description);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.getMenu().getItem(1).setChecked(true);
        Bundle bundle = getIntent().getExtras();
        getUserInformation();

        if (bundle != null) {
            String ingredient_name = bundle.getString("INCIname");
            try {
                details(ingredient_name);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_search:
                        Intent intent_search = new Intent(IngredientDetailsActivity.this, HomePageActivity.class);
                        startActivity(intent_search);
                        break;
                    case R.id.action_camera:
                        Intent intent_camera = new Intent(IngredientDetailsActivity.this, CameraInstructionsActivity.class);
                        startActivity(intent_camera);
                        break;
                    case R.id.action_account:
                        Intent intent_account = new Intent(IngredientDetailsActivity.this, UserAccountActivity.class);
                        startActivity(intent_account);
                        break;
                }
                return true;
            }
        });
    }

    public void details(String ingredient_name) throws ParseException {
        ParseQuery<ParseObject> query_ingredient = ParseQuery.getQuery("Ingredient");
        query_ingredient.whereEqualTo("INCIname", ingredient_name);
        List<ParseObject> ingredient = query_ingredient.find();

        function = ingredient.get(0).getString("function");
        if (hairType.equals("greasy")) {
            if (ingredient.get(0).getInt("influenceOnOilyHair") == 0) {
                ingredient_details.add(negative + " Bad influence on your greasy hair");
            } else if (ingredient.get(0).getInt("influenceOnOilyHair") == 2) {
                ingredient_details.add(positive + " Good influence on your greasy hair");
            }
        } else if (hairType.equals("dry")) {
            if (ingredient.get(0).getInt("influenceOnDryHair") == 0) {
                ingredient_details.add(negative + " Bad influence on your dry hair");
            } else if (ingredient.get(0).getInt("influenceOnDryHair") == 2) {
                ingredient_details.add(positive + " Good influence on your dry hair");
            }
        }
        if (skinType.equals("oily")) {
            if (ingredient.get(0).getInt("influenceOnOilySkin") == 0) {
                ingredient_details.add(negative + " Bad influence on your oily skin");
            } else if (ingredient.get(0).getInt("influenceOnOilySkin") == 2) {
                ingredient_details.add(positive + " Good influence on your oily skin");
            }
        } else if (skinType.equals("dry")) {
            if (ingredient.get(0).getInt("influenceOnDrySkin") == 0) {
                ingredient_details.add(negative + " Bad influence on your dry skin");
            } else if (ingredient.get(0).getInt("influenceOnDrySkin") == 2) {
                ingredient_details.add(positive + " Good influence on your dry skin");
            }
        } else if (skinType.equals("sensitive")) {
            if (ingredient.get(0).getInt("influenceOnSensitiveSkin") == 0) {
                ingredient_details.add(negative + " Bad influence on your sensitive skin");
            } else if (ingredient.get(0).getInt("influenceOnSensitiveSkin") == 2) {
                ingredient_details.add(positive + " Good influence on your sensitive skin");
            }
        } else if (skinType.equals("combination")) {
            if (ingredient.get(0).getInt("influenceOnCombinationSkin") == 0) {
                ingredient_details.add(negative + " Bad influence on your combination skin");
            } else if (ingredient.get(0).getInt("influenceOnCombinationSkin") == 2) {
                ingredient_details.add(positive + " Good influence on your combination skin");
            }
        }

        if (ifAcne) {
            if (ingredient.get(0).getInt("influenceOnAcneProne") == 0) {
                ingredient_details.add(negative + " Bad influence on your acne prone skin");
            } else if (ingredient.get(0).getInt("influenceOnAcneProne") == 2) {
                ingredient_details.add(positive + " Good influence on your acne prone skin");
            }
        }
        if (ifDermatitis) {
            if (ingredient.get(0).getInt("influenceOnAtopicDermatitis") == 0) {
                ingredient_details.add(negative + " Bad influence on your atopic dermatitis");
            } else if (ingredient.get(0).getInt("influenceOnAtopicDermatitis") == 2) {
                ingredient_details.add(positive + " Good influence on your atopic dermatitis");
            }
        }
        if (ifDandruff) {
            if (ingredient.get(0).getInt("influenceOnDandruff") == 0) {
                ingredient_details.add(negative + " Bad influence on your dandruff");
            } else if (ingredient.get(0).getInt("influenceOnDandruff") == 2) {
                ingredient_details.add(positive + " Good influence on your dandruff");
            }
        }
        if (ifAlcohols) {
            if (ingredient.get(0).getInt("ifAlcohol") == 1) {
                ingredient_details.add(negative + " This ingredient is an alcohol");
            } else {
                ingredient_details.add(positive + " This ingredient is not an alcohol");
            }
        }
        if (ifSilicones) {
            if (ingredient.get(0).getInt("ifSilicone") == 1) {
                ingredient_details.add(negative + " This ingredient is a silicone");
            } else {
                ingredient_details.add(positive + " This ingredient is not a silicone");
            }
        }
        if (ifBenzylDerivatives) {
            if (ingredient.get(0).getInt("ifBenzylDerivative") == 1) {
                ingredient_details.add(negative + " This ingredient is a benzyl derivative");
            } else {
                ingredient_details.add(positive + " This ingredient is not a benzyl derivative");
            }
        }
        if (ifParabens) {
            if (ingredient.get(0).getInt("ifParaben") == 1) {
                ingredient_details.add(negative + " This ingredient is a paraben");
            } else {
                ingredient_details.add(positive + " This ingredient is not a paraben");
            }
        }
        if (ifAnimalOrigin) {
            if (ingredient.get(0).getInt("ifAnimalOrigin") == 1) {
                ingredient_details.add(negative + " This ingredient is animal origin");
            } else {
                ingredient_details.add(positive + " This ingredient is not animal origin");
            }
        }
        if (ifPopularAllergens) {
            if (ingredient.get(0).getInt("ifPopularAllergen") == 1) {
                ingredient_details.add(negative + " This ingredient is a popular allergen");
            } else {
                ingredient_details.add(positive + " This ingredient is not a popular allergen");
            }
        }
        if (ifAllAllergens) {
            if (ingredient.get(0).getInt("ifAllergen") == 1) {
                ingredient_details.add(negative + " This ingredient is an allergen");
            } else {
                ingredient_details.add(positive + " This ingredient is not an allergen");
            }
        }
        if (ifDangerousForPregnant) {
            if (ingredient.get(0).getInt("ifDangerousForPregnant") == 1) {
                ingredient_details.add(negative + " This ingredient is dangerous for pregnant");
            } else {
                ingredient_details.add(positive + " This ingredient is not dangerous for pregnant");
            }
        }

        ingredientName.setText(ingredient_name);
//                    for(int i=0; i<ingredient_details.size(); i++){
//                        ingredientDetails.setText(ingredient_details.get(i));
//                        details+=ingredient_details.get(i).toString()+"\n";
//                    }
        ArrayAdapter<String> aa = new ArrayAdapter<String>(IngredientDetailsActivity.this, android.R.layout.simple_list_item_1, ingredient_details);
        listIngredientDetails.setAdapter(aa);
//                    ingredientDetails.setText(details);
//                }
//            }
//        });

        if (function == null) {
            slideView(ingredientDetails, ingredientDetails.getLayoutParams().height, 1);
            slideView(functionDescription, functionDescription.getLayoutParams().height, 1);
        } else {
            String[] functions = function.split(",");
            System.out.println(Arrays.toString(functions));
            details += functions[0];
            ingredientDetails.setText(details);
            ParseQuery<ParseObject> query_function_description = ParseQuery.getQuery("FunctionExplanation");
            query_function_description.whereEqualTo("function", functions[0]);
            List<ParseObject> description = query_function_description.find();
            System.out.println(function);
            functionDescription.setText(description.get(0).getString("description"));
        }

    }

    public static void slideView(View view,
                                 int currentHeight,
                                 int newHeight) {

        ValueAnimator slideAnimator = ValueAnimator
                .ofInt(currentHeight, newHeight)
                .setDuration(100);

        /* We use an update listener which listens to each tick
         * and manually updates the height of the view  */

        slideAnimator.addUpdateListener(animation1 -> {
            Integer value = (Integer) animation1.getAnimatedValue();
            view.getLayoutParams().height = value;
            view.requestLayout();
        });

        /*  We use an animationSet to play the animation  */

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.play(slideAnimator);
        animationSet.start();
    }


    public void getUserInformation() {
        hairType = ((String) ParseUser.getCurrentUser().get("hairType"));
        skinType = ((String) ParseUser.getCurrentUser().get("skinType"));

        ifAcne = (boolean) ParseUser.getCurrentUser().get("ifAcne");
        ifDermatitis = (boolean) ParseUser.getCurrentUser().get("ifDermatitis");
        ifDandruff = (boolean) ParseUser.getCurrentUser().get("ifDandruff");
        ifAlcohols = (boolean) ParseUser.getCurrentUser().get("ifAlcohols");
        ifSilicones = (boolean) ParseUser.getCurrentUser().get("ifSilicones");
        ifBenzylDerivatives = (boolean) ParseUser.getCurrentUser().get("ifBenzylDerivatives");
        ifParabens = (boolean) ParseUser.getCurrentUser().get("ifParabens");
        ifAnimalOrigin = (boolean) ParseUser.getCurrentUser().get("ifAnimalOrigin");
        ifPopularAllergens = (boolean) ParseUser.getCurrentUser().get("ifPopularAllergens");
        ifAllAllergens = (boolean) ParseUser.getCurrentUser().get("ifAllAllergens");
        ifDangerousForPregnant = (boolean) ParseUser.getCurrentUser().get("ifDangerousForPregnant");

    }

}
