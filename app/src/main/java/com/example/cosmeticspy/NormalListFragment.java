package com.example.cosmeticspy;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class NormalListFragment extends Fragment {
    ListView lv;
    ArrayAdapter<String> aa;
    ArrayList<String> products_with_rate2 = new ArrayList<>();
    ArrayList<String> substitutes_list = new ArrayList<>();
    String category;

    public NormalListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle= getArguments();
        if (bundle != null){
            category = bundle.getString("Category");
            substitutes_list = (ArrayList<String>) bundle.getSerializable("Substitutes");
            products_with_rate2 = (ArrayList<String>) bundle.getSerializable("ProductsWithRate2");
        }
        View v = inflater.inflate(R.layout.fragment_normal_list, container, false);
        lv = (ListView) v.findViewById(R.id.product_substitutes);

        aa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, substitutes_list);
        lv.setAdapter(aa);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            String wholeText = substitutes_list.get(position);
            String[] separated = wholeText.split(" -- ");
            Intent intent_product_details = new Intent(view.getContext(), ProductDetailsActivity.class);
            intent_product_details.putExtra("ProductName", separated[1]);
            intent_product_details.putExtra("ProductPoints", separated[0]);
            intent_product_details.putExtra("ProductCategory", category);
            System.out.println(products_with_rate2);
            Bundle args = new Bundle();
            args.putSerializable("ARRAYLIST",(Serializable)products_with_rate2);
            intent_product_details.putExtra("BUNDLE",args);
            startActivity(intent_product_details);
        });
        return v;
    }
}

