package com.example.cosmeticspy;

public class Product implements Comparable<Product> {
    private String product_name;
    private int score;
    private float sim;

    public Product(String product_name, int score, float sim) {
        this.product_name = product_name;
        this.score = score;
        this.sim = sim;
    }

    public String getProductName() {
        return product_name;
    }

    public int getScore() {
        return score;
    }

    public float getSim() {
        return sim;
    }

    public void setSim(float sim) {
        this.sim = sim;
    }

    @Override
    public int compareTo(Product o) {
        return (int) (o.sim*1000 - this.sim*1000);
    }
}