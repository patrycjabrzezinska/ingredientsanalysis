package com.example.cosmeticspy;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class UserAccountActivity extends AppCompatActivity {

    TextView name, surname, email, username, gender, hairtype, skintype;
    ImageView imageAcne, imageDermatitis, imageDandruff, imageSpecificAllergens, imageAlcohols, imageSilicones, imageBenzyl, imageParabens, imageAnimal, imagePopularAllergens, imageAllAllergens, imagePregnant;
    boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives, ifParabens, ifPopularAllergens, ifAllAllergens, ifAnimalOrigin, ifDangerousForPregnant  = false;
    ArrayList<String> allergens_list  = new ArrayList<>();
    ArrayList<String> list_added, list_deleted  = new ArrayList<>();
    ParseQuery<ParseObject> avoidAllergenListquery, allergensToDelete;
    ArrayAdapter<String> arrayAdapter;
    ListView allergensListView;
    int ifDelete = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.getMenu().getItem(2).setChecked(true);


        name = (TextView) findViewById(R.id.name);
        name.setText( (String) ParseUser.getCurrentUser().get("name"));
        surname = (TextView) findViewById(R.id.surname);
        surname.setText( (String) ParseUser.getCurrentUser().get("surname"));
        email = (TextView) findViewById(R.id.email);
        email.setText( (String) ParseUser.getCurrentUser().get("email"));
        username = (TextView) findViewById(R.id.username);
        username.setText( (String) ParseUser.getCurrentUser().get("username"));
        gender = (TextView) findViewById(R.id.gender);
        gender.setText( (String) ParseUser.getCurrentUser().get("gender"));
        hairtype = (TextView) findViewById(R.id.hairtype);
        hairtype.setText( (String) ParseUser.getCurrentUser().get("hairType"));
        skintype = (TextView) findViewById(R.id.skintype);
        skintype.setText( (String) ParseUser.getCurrentUser().get("skinType"));
        allergensListView = (ListView) findViewById(R.id.list_of_allergens);

        ifAcne = (boolean) ParseUser.getCurrentUser().get("ifAcne");
        ifDermatitis = (boolean) ParseUser.getCurrentUser().get("ifDermatitis");
        ifDandruff = (boolean) ParseUser.getCurrentUser().get("ifDandruff");
        ifAlcohols = (boolean) ParseUser.getCurrentUser().get("ifAlcohols");
        ifSilicones = (boolean) ParseUser.getCurrentUser().get("ifSilicones");
        ifBenzylDerivatives = (boolean) ParseUser.getCurrentUser().get("ifBenzylDerivatives");
        ifParabens = (boolean) ParseUser.getCurrentUser().get("ifParabens");
        ifAnimalOrigin = (boolean) ParseUser.getCurrentUser().get("ifAnimalOrigin");
        ifPopularAllergens = (boolean) ParseUser.getCurrentUser().get("ifPopularAllergens");
        ifAllAllergens = (boolean) ParseUser.getCurrentUser().get("ifAllAllergens");
        ifDangerousForPregnant = (boolean) ParseUser.getCurrentUser().get("ifDangerousForPregnant");

        ParseUser user1 = ParseUser.getCurrentUser();
        String id = user1.getObjectId();
        avoidAllergenListquery = ParseQuery.getQuery("AvoidAllergen");
        avoidAllergenListquery.whereEqualTo("UserId", id);
        avoidAllergenListquery.findInBackground((currentAllergens, e) -> {
            if (e == null) {
                for (int i = 0; i < currentAllergens.size(); i++) {
                    String allergen = currentAllergens.get(i).get("AllergenName").toString();
                    //if (!allergens_list.contains(allergen)) {
                    allergens_list.add(allergen);
                    //}
                }
                if (allergens_list.size()>0) {
                    imageSpecificAllergens = findViewById(R.id.specific_allergy);
                    imageSpecificAllergens.setImageResource(R.drawable.ok);
                }
                arrayAdapter = new ArrayAdapter<>(UserAccountActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, allergens_list);
                allergensListView.setAdapter(arrayAdapter);
                setListViewHeightBasedOnItems(allergensListView);
            } else {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        if (ifAcne) {
            imageAcne = findViewById(R.id.acne_prone);
            imageAcne.setImageResource(R.drawable.ok);
        }
        if (ifDermatitis) {
            imageDermatitis = findViewById(R.id.dermatitis);
            imageDermatitis.setImageResource(R.drawable.ok);
        }
        if (ifDandruff) {
            imageDandruff = findViewById(R.id.dandruff);
            imageDandruff.setImageResource(R.drawable.ok);
        }

        if (ifAlcohols) {
            imageAlcohols = findViewById(R.id.alcohols);
            imageAlcohols.setImageResource(R.drawable.ok);
        }
        if (ifSilicones) {
            imageSilicones = findViewById(R.id.silicones);
            imageSilicones.setImageResource(R.drawable.ok);
        }
        if (ifBenzylDerivatives) {
            imageBenzyl = findViewById(R.id.benzyl_derivatives);
            imageBenzyl.setImageResource(R.drawable.ok);
        }
        if (ifParabens) {
            imageParabens = findViewById(R.id.parabens);
            imageParabens.setImageResource(R.drawable.ok);
        }
        if (ifAnimalOrigin) {
            imageAnimal = findViewById(R.id.animal_origin);
            imageAnimal.setImageResource(R.drawable.ok);
        }
        if (ifPopularAllergens) {
            imagePopularAllergens = findViewById(R.id.popular_allergens);
            imagePopularAllergens.setImageResource(R.drawable.ok);
        }
        if (ifAllAllergens) {
            imageAllAllergens = findViewById(R.id.all_allergens);
            imageAllAllergens.setImageResource(R.drawable.ok);
        }
        if (ifDangerousForPregnant) {
            imagePregnant = findViewById(R.id.dangerous_for_pregnant);
            imagePregnant.setImageResource(R.drawable.ok);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_search:
                        Intent intentSearch = new Intent(UserAccountActivity.this, HomePageActivity.class);
                        startActivity(intentSearch);
                        break;
                    case R.id.action_camera:
                        Intent intent = new Intent(UserAccountActivity.this, CameraInstructionsActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_account:
                        Intent intentUser = new Intent(UserAccountActivity.this, UserAccountActivity.class);
                        startActivity(intentUser);
                        break;
                }
                return true;
            }
        });
    }


    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    public void deleteUser(View view) throws InterruptedException {
        ParseUser currentUser = ParseUser.getCurrentUser();
        //Intent intent = new Intent(UserAccountActivity.this, LoginActivity.class);
        //startActivity(intent);
        if (currentUser != null) {

            showDialog(UserAccountActivity.this, "Are you sure?");

            }

        //ifDelete = 0;
    }

    public void deleteAccount() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        Intent intent = new Intent(UserAccountActivity.this, LoginActivity.class);
        startActivity(intent);
        currentUser.deleteInBackground(e -> {
            if(e==null){
                Toast.makeText(this, "User was deleted", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void showDialog(Activity activity, CharSequence message) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create(); //Read Update
        alertDialog.setTitle("Confirm");
        alertDialog.setMessage(message);
        alertDialog.setButton( Dialog.BUTTON_POSITIVE, "Yes, delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //ifDelete = 1;
                deleteAccount();
                alertDialog.dismiss();
            }
        });

        alertDialog.setButton( Dialog.BUTTON_NEGATIVE, "No, keep my account", new DialogInterface.OnClickListener()    {
            public void onClick(DialogInterface dialog, int which) {
                ifDelete = 2;
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }



    public void editUser(View view) {
        Intent intent = new Intent(UserAccountActivity.this, EditUserActivity.class);
        //intent.putExtras(intent.putExtra("mode", "edit"));
        //setResult(RESULT_OK, retData);
        startActivity(intent);
    }

    public void editListOfAllergens(View view) {
        Intent intent = new Intent(UserAccountActivity.this, AllergenPopupLayoutActivity.class);
        intent.putExtras(intent.putExtra("mode", "edit"));
        //setResult(RESULT_OK, retData);
        startActivityForResult(intent, 1);
        //startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("*************** request code *******************");
        System.out.println(requestCode);
        String result = "cancel";
        if(requestCode == 1) {
            assert data != null;
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                allergens_list = bundle.getStringArrayList("list_of_allergens");
                list_added = bundle.getStringArrayList("list_added");
                list_deleted = bundle.getStringArrayList("list_deleted");
                result = bundle.getString("result");
            }
            if (result.equals("ok")) {
                ParseUser user1 = ParseUser.getCurrentUser();
                String id = user1.getObjectId();
                if (!list_added.isEmpty()) {
                    for (String element : list_added) {
                        ParseObject entity = new ParseObject("AvoidAllergen");
                        entity.put("AllergenName", element);
                        entity.put("UserId", id);
                        entity.saveInBackground(e -> {
                            if (e == null) {
                                Toast.makeText(UserAccountActivity.this, "Success", Toast.LENGTH_LONG).show();
                            } else {
                                //Something went wrong
                                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        System.out.println("***************added element *******************");
                        System.out.println(element);
                        arrayAdapter.insert(element, arrayAdapter.getCount());
                        setListViewHeightBasedOnItems(allergensListView);
                    }
                }
                if (!list_deleted.isEmpty()) {
                    allergensToDelete = ParseQuery.getQuery("AvoidAllergen");
                    allergensToDelete.whereEqualTo("UserId", id);
                    //allergensToDelete.whereEqualTo("AllergenName", element);
                    allergensToDelete.findInBackground((currentAllergens, e) -> {
                        if (e == null) {
                            for (int i = 0; i < currentAllergens.size(); i++) {
                                if (list_deleted.contains(currentAllergens.get(i).get("AllergenName").toString())) {
                                    System.out.println("***************deleted element *******************");
                                    System.out.println(currentAllergens.get(i).get("AllergenName").toString());
                                    currentAllergens.get(i).deleteInBackground();
                                    arrayAdapter.remove(currentAllergens.get(i).get("AllergenName").toString());
                                    setListViewHeightBasedOnItems(allergensListView);
                                }
                            }
                            setListViewHeightBasedOnItems(allergensListView);
                        } else {
                            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        }

    }

    public void logout(View view) {
        ParseUser.getCurrentUser();
        ParseUser.logOut();
        Intent intent = new Intent(UserAccountActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}