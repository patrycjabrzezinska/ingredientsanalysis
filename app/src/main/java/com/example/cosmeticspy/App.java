package com.example.cosmeticspy;

import android.app.Application;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;

import auxiliaryClasses.AnalysisData;

public class App extends Application {

    ParseQuery<ParseObject> ingredientsListquery;
    ParseQuery<ParseObject> ingredientsListqueryAll;

    ArrayList<String> allIngredientsNames = new ArrayList<>();
    ArrayList<ParseObject> allIngredientsAll = new ArrayList<ParseObject>();

    ParseQuery<ParseObject> productsListQuery;
    ArrayList<ParseObject> allProducts = new ArrayList<ParseObject>();

    AnalysisData analysisData = new AnalysisData();
    String userLastUpdate = "";
    boolean ifAllergyUpdate = false;
    boolean ifIngredientsLoaded = false;



    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.back4app_app_id))
                .clientKey(getString(R.string.back4app_client_key))
                .server(getString(R.string.back4app_server_url))
                .build());
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseUser.getCurrentUser();
        ParseUser.logOut();

        ingredientsListquery = ParseQuery.getQuery("Ingredient");
        ingredientsListquery.selectKeys(Collections.singleton("INCIname"));
        ingredientsListquery.setLimit(30000);
        ingredientsListqueryAll = ParseQuery.getQuery("Ingredient");
        ingredientsListqueryAll.setLimit(30000);
        //search_ingredients();
        search_ingredients2();

        productsListQuery = ParseQuery.getQuery("Product");
        productsListQuery.setLimit(2500);
        productsListQuery.include("ProductCategory");

        search_products();

    }

    public ArrayList<String> getAllIngredientsNames() {
        return allIngredientsNames;
    }

    public ArrayList<ParseObject> getAllIngredientsAll() {
        return allIngredientsAll;
    }

    public ArrayList<ParseObject> getAllProducts() {
        return allProducts;
    }

    public boolean getIfIngredientsLoaded() {
        return ifIngredientsLoaded;
    }

    public void search_ingredients2() {
        long startTime = System.currentTimeMillis();
        ingredientsListqueryAll.findInBackground((objects, e) -> {
            if (e == null) {
                for (int i = 0; i < objects.size(); i++) {
                    allIngredientsAll.add(objects.get(i));
                }
            } else {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            long endTime = System.currentTimeMillis();
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("That took " + (endTime - startTime) + " milliseconds - app");
            ifIngredientsLoaded = true;

        });
    }

    private void search_products() {
        long startTime = System.currentTimeMillis();
        productsListQuery.findInBackground((objects, e) -> {
            if (e == null) {
                allProducts.addAll(objects);
            } else {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            long endTime = System.currentTimeMillis();
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("Loading products took " + (endTime - startTime) + " milliseconds - app");
            //System.out.println(allProducts.get(1));

        });
    }

    public AnalysisData getAnalysisData() {
        return analysisData;
    }

    public void setUserLastUpdate(String userLastUpdate) {
        this.userLastUpdate = userLastUpdate;
    }

    public String getUserLastUpdate() {
        return userLastUpdate;
    }

    public boolean isIfAllergyUpdate() {
        return ifAllergyUpdate;
    }

    public void setIfAllergyUpdate(boolean ifAllergyUpdate) {
        this.ifAllergyUpdate = ifAllergyUpdate;
    }
}

