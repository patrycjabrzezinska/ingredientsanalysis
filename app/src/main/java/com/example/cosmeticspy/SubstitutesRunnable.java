package com.example.cosmeticspy;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.parse.Parse.getApplicationContext;

public class SubstitutesRunnable implements Runnable{
    private static final Object lock = new Object();
    private ArrayList<String> productsAnalysed;
    private ArrayList<Product> productsToAnalyse;
    private ArrayList<String> init_ingredients;

    public SubstitutesRunnable(ArrayList<String> productsAnalysed, ArrayList<Product> productsToAnalyse, ArrayList<String> init_ingredients) {
        this.productsAnalysed = productsAnalysed;
        this.productsToAnalyse = productsToAnalyse;
        this.init_ingredients = init_ingredients;
    }

    @Override
    public void run() {
        final App application = (App) getApplicationContext();
        ArrayList<ParseObject> allProducts = application.getAllProducts();
        for (int i = 0; i < productsToAnalyse.size(); i++) {
            String product_name = (productsToAnalyse.get(i).getProductName());
            ArrayList<String> found_ingredients = new ArrayList<>();
            ParseObject product = null;
            for (ParseObject elem : allProducts) {
                if (elem.get("Name").equals(product_name)) {
                    product = elem;
                    break;
                }
            }
            assert product != null;

            ParseRelation<ParseObject> ingredient_relation = product.getRelation("ConsistsOf");
            List<ParseObject> obj_ingredients = null;
            try {
                obj_ingredients = ingredient_relation.getQuery().find();
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }

            for (int j = 0; j < obj_ingredients.size(); j++) {
                found_ingredients.add(obj_ingredients.get(j).getString("INCIname"));
            }
            System.out.println(intersect(init_ingredients, found_ingredients));
            List<String> ing = intersect(init_ingredients, found_ingredients);
            float calculated = ((float) ing.size()) / ((float) init_ingredients.size() + (float) found_ingredients.size());

            synchronized (lock) {
                productsToAnalyse.get(i).setSim(calculated);
                System.out.println(Thread.currentThread().getName() + " is now updating global list");
                System.out.println(calculated+" -- "+product_name);
                System.out.println(Thread.currentThread().getName() + " finished updating global list");
            }
        }
    }

    private List<String> intersect(List<String> A, List<String> B) {
        List<String> rtnList = new LinkedList<>();
        for(String dto : A) {
            if(B.contains(dto)) {
                rtnList.add(dto);
            }
        }
        return rtnList;
    }

}
