package com.example.cosmeticspy;

import android.widget.ArrayAdapter;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseRelation;

import java.util.ArrayList;
import java.util.List;

import auxiliaryClasses.Ingredient;

import static com.parse.Parse.getApplicationContext;

public class AnalysisRunnable implements Runnable {

    private static final Object lock = new Object();
    private ArrayList<String> productsAnalysed;
    private ArrayList<String> productsAnalysedLocal = new ArrayList<>();
    private ArrayList<ParseObject> productsToAnalyse;
    private boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives, ifParabens, ifPopularAllergens, ifAllAllergens, ifAnimalOrigin, ifDangerousForPregnant;
    private String skinType, hairType;
    private ArrayList<String> avoidAllergen;
    String category;

    public AnalysisRunnable(ArrayList<String> productsAnalysed, ArrayList<ParseObject> productsToAnalyse,
                            boolean ifAcne, boolean ifDermatitis, boolean ifDandruff, boolean ifAlcohols,
                            boolean ifSilicones, boolean ifBenzylDerivatives, boolean ifParabens,
                            boolean ifPopularAllergens, boolean ifAllAllergens, boolean ifAnimalOrigin,
                            boolean ifDangerousForPregnant, String skinType, String hairType, ArrayList<String> avoidAllergen,String category) {
        this.productsAnalysed = productsAnalysed;
        this.productsToAnalyse = productsToAnalyse;
        this.ifAcne = ifAcne;
        this.ifDermatitis = ifDermatitis;
        this.ifDandruff = ifDandruff;
        this.ifAlcohols = ifAlcohols;
        this.ifSilicones = ifSilicones;
        this.ifBenzylDerivatives = ifBenzylDerivatives;
        this.ifParabens = ifParabens;
        this.ifPopularAllergens = ifPopularAllergens;
        this.ifAllAllergens = ifAllAllergens;
        this.ifAnimalOrigin = ifAnimalOrigin;
        this.ifDangerousForPregnant = ifDangerousForPregnant;
        this.skinType = skinType;
        this.hairType = hairType;
        this.avoidAllergen = avoidAllergen;
        this.category = category;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " starts analysing its subset");

        for (ParseObject product : productsToAnalyse) {
            int numberOfBadIng = 0;
            int numberOfGoodIng = 0;
            int numberOfNeutralIng = 0;
            boolean ifAllergy = false;
            ArrayList<Ingredient> ingredients = new ArrayList<>();

            long startTime2 = System.currentTimeMillis();
            ParseRelation<ParseObject> ingredient_relation = product.getRelation("ConsistsOf");
            List<ParseObject> obj_ingredients = null;
            try {
                obj_ingredients = ingredient_relation.getQuery().find();
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }


            for (int k = 0; k < obj_ingredients.size(); k++) {
                ingredients.add(new Ingredient(obj_ingredients.get(k).getString("INCIname"),
                        obj_ingredients.get(k).getInt("ifParaben"),
                        obj_ingredients.get(k).getInt("ifAllergen"),
                        obj_ingredients.get(k).getInt("ifPopularAllergen"),
                        obj_ingredients.get(k).getInt("ifAnimalOrigin"),
                        obj_ingredients.get(k).getInt("ifBenzylDerivative"),
                        obj_ingredients.get(k).getInt("ifAlcohol"),
                        obj_ingredients.get(k).getInt("ifSilicone"),
                        obj_ingredients.get(k).getInt("ifDangerousForPregnant"),
                        obj_ingredients.get(k).getInt("influenceOnOilySkin"),
                        obj_ingredients.get(k).getInt("influenceOnDrySkin"),
                        obj_ingredients.get(k).getInt("influenceOnCombinationSkin"),
                        obj_ingredients.get(k).getInt("influenceOnAcneProne"),
                        obj_ingredients.get(k).getInt("influenceOnSensitiveSkin"),
                        obj_ingredients.get(k).getInt("influenceOnDryHair"),
                        obj_ingredients.get(k).getInt("influenceOnOilyHair"),
                        obj_ingredients.get(k).getInt("influenceOnDandruff"),
                        obj_ingredients.get(k).getInt("influenceOnAtopicDermatitis")));
                long endTime2 = System.currentTimeMillis();
                System.out.println("XXXXXXXXXXXXXXXX get relations XXXXXXXXXXXXXXXXXX");
                System.out.println("That took " + (endTime2 - startTime2) + " milliseconds");
                boolean ifAllergyToIngredient = false;

                if (category.equals("Hair dye") || category.equals("Hair spray")
                        || category.equals("Hair") || category.equals("Shampoo")
                        || category.equals("Conditioner")) {
                    ingredients.get(k).countHairType(hairType);
                    if (ifDandruff) {
                        ingredients.get(k).countIfDandruff();
                    }
                }
                if (category.equals("Facial skincare") || category.equals("Body skincare")
                        || category.equals("Anti aging") || category.equals("Foot cream&lotion")
                        || category.equals("Lip balm&cream") || category.equals("Skincare")
                        || category.equals("Body moisturiser") || category.equals("Eye cream")
                        || category.equals("Suncream") || category.equals("Hand cream&lotion")
                        || category.equals("Face moisturiser") || category.equals("Shower gel")) {
                    ingredients.get(k).countSkinType(skinType);
                    if (ifAcne) {
                        ingredients.get(k).countIfAcne();
                    }
                    if (ifDermatitis) {
                        ingredients.get(k).countIfDermatitis();
                    }
                }

                if (ifAlcohols) {
                    ingredients.get(k).countIfAlcohol();
                }

                if (ifSilicones) {
                    ingredients.get(k).countIfSilicone();
                }
                if (ifBenzylDerivatives) {
                    ingredients.get(k).countIfBenzylDerivative();
                }

                if (ifParabens) {
                    ingredients.get(k).countIfParaben();
                }

                if (ifAnimalOrigin) {
                    ingredients.get(k).countIfAnimalOrigin();
                }

                if (ifPopularAllergens) {
                    ingredients.get(k).countIfPopularAllergen();
                }

                if (ifAllAllergens) {
                    ingredients.get(k).countIfAllAllergen();
                }
                if (ifDangerousForPregnant) {
                    ingredients.get(k).countIfDangerousForPregnant();
                }
                ifAllergyToIngredient = ingredients.get(k).checkIfUserAllergen(avoidAllergen);
                ifAllergy = ifAllergyToIngredient;
                ingredients.get(k).countIngredientAverage();
                if(ifAllergyToIngredient){
                    break;
                }
                if (ingredients.get(k).getPoints() < 0.9) {
                    numberOfBadIng++;
                } else if (ingredients.get(k).getPoints() > 1.1) {
                    numberOfGoodIng++;
                } else {
                    numberOfNeutralIng++;
                }

            }
            Double finalRate = 0.0;
            if(!ifAllergy) {
                finalRate = (double) 50 * ((numberOfGoodIng * 20) + numberOfNeutralIng) / ((numberOfGoodIng * 10) + (numberOfBadIng * 10) + numberOfNeutralIng);
            }
            synchronized (lock) {
                //System.out.println(Thread.currentThread().getName()+" is now updating global list");
                productsAnalysed.add("<b>" + finalRate.intValue() + "%" + "</b>" + " -- " + product.getString("Name"));
                //System.out.println(Thread.currentThread().getName()+" finished updating global list");

            }

        }
    }
}
