package com.example.cosmeticspy;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListFragment extends Fragment {
    ExpandableListView lv;
    String product_name ;
    ArrayList<String> ingredientsNames = new ArrayList<>();
    MyExpandableListAdapter arrayIngredientsAdapter;
    HashMap<String, List<String>> listIngredientDetails = new HashMap<>();

    public ExpandableListFragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle= getArguments();
        if (bundle != null){
            product_name = bundle.getString("ProductName");
            ingredientsNames = (ArrayList<String>) bundle.getSerializable("IngredientsNames");
            listIngredientDetails = (HashMap<String, List<String>>)bundle.getSerializable("ListIngredientDetails");
        }

        View v = inflater.inflate(R.layout.fragment_expandable_list, container, false);
        lv = v.findViewById(R.id.product_ingredients);
        arrayIngredientsAdapter = new MyExpandableListAdapter(getActivity(), ingredientsNames, listIngredientDetails);
        lv.setAdapter(arrayIngredientsAdapter);
        if(lv.getParent() != null) {
            ((ViewGroup)lv.getParent()).removeView(lv);
        }
        return lv;
    }
}