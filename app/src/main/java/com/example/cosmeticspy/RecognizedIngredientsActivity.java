package com.example.cosmeticspy;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import SpellCorrector.ISpellCorrector;
import SpellCorrector.SpellCorrector;
import auxiliaryClasses.Ingredient;
import auxiliaryClasses.MyExpandableListAdapter;

public class RecognizedIngredientsActivity extends AppCompatActivity {

    TextView productPoints;
    boolean ifAcne, ifDermatitis, ifDandruff, ifAlcohols, ifSilicones, ifBenzylDerivatives, ifParabens, ifPopularAllergens, ifAllAllergens, ifAnimalOrigin, ifDangerousForPregnant;
    String skinType, hairType;
    ArrayList<String> avoidAllergens = new ArrayList<>();
    String category = "";
    int numberOfGoodIng = 0;
    int numberOfBadIng = 0;
    int numberOfNeutralIng = 0;
    ExpandableListView productIngredients;
    ArrayList<String> ingredientsNames = new ArrayList<>();
    HashMap<String, List<String>> listIngredientDetails = new HashMap<>();
    ArrayList<Ingredient> ingredients = new ArrayList<>();
    MyExpandableListAdapter arrayIngredientsAdapter;

    private ArrayList<String> possibleIngredients = new ArrayList<>();
    private ArrayList<String> detectedIngredients = new ArrayList<>();
    private ISpellCorrector corrector;
    private String detectedText;
    private Bitmap photo;
    private ProgressBar progressBar;
    LinearLayout linlaHeaderProgress;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recognized_ingredients);
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        productPoints = (TextView) findViewById(R.id.product_points);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.getMenu().getItem(0).setChecked(true);
        productIngredients = (ExpandableListView) findViewById(R.id.detected_ingredients);
        progressBar = (ProgressBar) findViewById(R.id.simpleProgressBar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_search:
                        Intent intent_search = new Intent(RecognizedIngredientsActivity.this, HomePageActivity.class);
                        startActivity(intent_search);
                        break;
                    case R.id.action_camera:
                        Intent intent_camera = new Intent(RecognizedIngredientsActivity.this, CameraInstructionsActivity.class);
                        startActivity(intent_camera);
                        break;
                    case R.id.action_account:
                        Intent intent_account = new Intent(RecognizedIngredientsActivity.this, UserAccountActivity.class);
                        startActivity(intent_account);
                        break;
                }
                return true;
            }
        });
        doAnalysis();


    }

    public void doAnalysis(){
        new AsyncTask<Void, Void, Void>() {
            protected void onPreExecute() {
                super.onPreExecute();
                linlaHeaderProgress.setVisibility(View.VISIBLE);
            }



            @RequiresApi(api = Build.VERSION_CODES.N)
            protected Void doInBackground(Void... params) {
                Bundle bundle = getIntent().getExtras();
                category = bundle.getString("category");
                String uri = bundle.getString("photo");
                try {
                    photo = MediaStore.Images.Media.getBitmap(RecognizedIngredientsActivity.this.getContentResolver(), Uri.parse(uri));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                recognizeText();
                preparePossibleIngredientsNames();
                prepareSpellCorrector();
                extractIngredientsFromtext();

                getUserInformation();
                return null;

            }

            protected void onPostExecute(Void result) {
                linlaHeaderProgress.setVisibility(View.GONE);
                try {
                    prepareIngredientsList();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }.execute();

    }


    public void recognizeText() {

        TextRecognizer recognizer = new TextRecognizer.Builder(getApplicationContext()).build();

        if (recognizer.isOperational() != true) {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        } else {
            Frame frame = new Frame.Builder().setBitmap(photo).build();
            SparseArray<TextBlock> items = recognizer.detect(frame);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < items.size(); i++) {
                TextBlock myItem = items.valueAt(i);
                sb.append(myItem.getValue());
                sb.append("\n");
            }
            detectedText = sb.toString();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void preparePossibleIngredientsNames(){
        //take ingredients from the database and put in arraylist, then sort according to lenght descendingly
        final App application = (App) getApplicationContext();
        ArrayList<ParseObject> allIngredients = application.getAllIngredientsAll();
        for (int i = 0; i < allIngredients.size(); i++) {
            String ingredientName = allIngredients.get(i).getString("INCIname");
            possibleIngredients.add(ingredientName);
            if (allIngredients.get(i).getString("secondName").equals("none") == false) {
                String ingredientName2 = allIngredients.get(i).getString("secondName");
                possibleIngredients.add(ingredientName2);
            }
            if (allIngredients.get(i).getString("thirdName").equals("none") == false) {
                String ingredientName3 = allIngredients.get(i).getString("thirdName");
                possibleIngredients.add(ingredientName3);
            }
        }

        possibleIngredients.sort(Comparator.comparingInt(String::length).reversed());
    }

    public void prepareSpellCorrector(){
        //prepare a spell corrector
        InputStream input;
        try {
            input = getAssets().open("notsobig.txt");
        } catch (IOException e) {
            input = null;
            e.printStackTrace();
        }
        corrector = new SpellCorrector();
        try {
            corrector.useDictionary(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void extractIngredientsFromtext(){
        detectedText = detectedText.toLowerCase().replace("\n", " ");
        String[] words = detectedText.split(" ");
        detectedText = "";
        for (int i = 0; i < words.length; i++) {
            String correctedWord = corrector.suggestSimilarWord(words[i]);
            if (correctedWord != null) {
                words[i] = correctedWord;
            }
            detectedText = detectedText + " " + words[i];
        }
        for (String name : possibleIngredients) {
            if (detectedText.contains(name.toLowerCase())) {
                if (countWords(name) == 1) {
                    if (detectedText.matches(".*\\b" + name.toLowerCase() + "\\b.*")) {
                        detectedIngredients.add(name);
                        detectedText = detectedText.replaceAll(name.toLowerCase(), "");
                    }
                } else {
                    detectedIngredients.add(name);
                    detectedText = detectedText.replaceAll(name.toLowerCase(), "");
                }
            }
        }

    }

    public int countWords(String str) {
        int state = 0;
        int wc = 0;  // word count
        int i = 0;

        // Scan all characters one by one
        while (i < str.length()) {
            // If next character is a separator, set the
            // state as OUT
            if (str.charAt(i) == ' ' || str.charAt(i) == '\n'
                    || str.charAt(i) == '\t')
                state = 0;


                // If next character is not a word separator
                // and state is OUT, then set the state as IN
                // and increment word count
            else if (state == 0) {
                state = 1;
                ++wc;
            }

            // Move to next character
            ++i;
        }
        return wc;
    }

    public void prepareIngredientsList() throws ParseException {
        final App application = (App) getApplicationContext();
        ArrayList<ParseObject> allIngredients = application.getAllIngredientsAll();
        boolean ifAllergy = false;
        for (String detectedIngredient : detectedIngredients) {
            for (int i = 0; i < allIngredients.size(); i++) {
                if (allIngredients.get(i).getString("INCIname").equals(detectedIngredient)) {
                    ingredients.add(new Ingredient(allIngredients.get(i).getString("INCIname"),
                            allIngredients.get(i).getInt("ifParaben"),
                            allIngredients.get(i).getInt("ifAllergen"),
                            allIngredients.get(i).getInt("ifPopularAllergen"),
                            allIngredients.get(i).getInt("ifAnimalOrigin"),
                            allIngredients.get(i).getInt("ifBenzylDerivative"),
                            allIngredients.get(i).getInt("ifAlcohol"),
                            allIngredients.get(i).getInt("ifSilicone"),
                            allIngredients.get(i).getInt("ifDangerousForPregnant"),
                            allIngredients.get(i).getInt("influenceOnOilySkin"),
                            allIngredients.get(i).getInt("influenceOnDrySkin"),
                            allIngredients.get(i).getInt("influenceOnCombinationSkin"),
                            allIngredients.get(i).getInt("influenceOnAcneProne"),
                            allIngredients.get(i).getInt("influenceOnSensitiveSkin"),
                            allIngredients.get(i).getInt("influenceOnDryHair"),
                            allIngredients.get(i).getInt("influenceOnOilyHair"),
                            allIngredients.get(i).getInt("influenceOnDandruff"),
                            allIngredients.get(i).getInt("influenceOnAtopicDermatitis")));
                    break;
                } else if (allIngredients.get(i).getString("secondName").equals(detectedIngredient)) {
                    ingredients.add(new Ingredient(allIngredients.get(i).getString("INCIname"),
                            allIngredients.get(i).getInt("ifParaben"),
                            allIngredients.get(i).getInt("ifAllergen"),
                            allIngredients.get(i).getInt("ifPopularAllergen"),
                            allIngredients.get(i).getInt("ifAnimalOrigin"),
                            allIngredients.get(i).getInt("ifBenzylDerivative"),
                            allIngredients.get(i).getInt("ifAlcohol"),
                            allIngredients.get(i).getInt("ifSilicone"),
                            allIngredients.get(i).getInt("ifDangerousForPregnant"),
                            allIngredients.get(i).getInt("influenceOnOilySkin"),
                            allIngredients.get(i).getInt("influenceOnDrySkin"),
                            allIngredients.get(i).getInt("influenceOnCombinationSkin"),
                            allIngredients.get(i).getInt("influenceOnAcneProne"),
                            allIngredients.get(i).getInt("influenceOnSensitiveSkin"),
                            allIngredients.get(i).getInt("influenceOnDryHair"),
                            allIngredients.get(i).getInt("influenceOnOilyHair"),
                            allIngredients.get(i).getInt("influenceOnDandruff"),
                            allIngredients.get(i).getInt("influenceOnAtopicDermatitis")));
                    break;
                } else if (allIngredients.get(i).getString("thirdName").equals(detectedIngredient)) {
                    ingredients.add(new Ingredient(allIngredients.get(i).getString("INCIname"),
                            allIngredients.get(i).getInt("ifParaben"),
                            allIngredients.get(i).getInt("ifAllergen"),
                            allIngredients.get(i).getInt("ifPopularAllergen"),
                            allIngredients.get(i).getInt("ifAnimalOrigin"),
                            allIngredients.get(i).getInt("ifBenzylDerivative"),
                            allIngredients.get(i).getInt("ifAlcohol"),
                            allIngredients.get(i).getInt("ifSilicone"),
                            allIngredients.get(i).getInt("ifDangerousForPregnant"),
                            allIngredients.get(i).getInt("influenceOnOilySkin"),
                            allIngredients.get(i).getInt("influenceOnDrySkin"),
                            allIngredients.get(i).getInt("influenceOnCombinationSkin"),
                            allIngredients.get(i).getInt("influenceOnAcneProne"),
                            allIngredients.get(i).getInt("influenceOnSensitiveSkin"),
                            allIngredients.get(i).getInt("influenceOnDryHair"),
                            allIngredients.get(i).getInt("influenceOnOilyHair"),
                            allIngredients.get(i).getInt("influenceOnDandruff"),
                            allIngredients.get(i).getInt("influenceOnAtopicDermatitis")));
                    break;
                }

            }
        }

        for (int i = 0; i < ingredients.size(); i++) {
            boolean ifAllergyToIngredient = false;

            if (category.contains("Hair dye") || category.contains("Hair spray")
                    || category.contains("Hair") || category.contains("Shampoo")
                    || category.contains("Conditioner")) {
                ingredients.get(i).countHairType(hairType);
                if (ifDandruff) {
                    ingredients.get(i).countIfDandruff();
                }
            } else if (category.contains("Facial skincare") || category.contains("Body skincare")
                    || category.contains("Anti aging") || category.contains("Foot cream&lotion")
                    || category.contains("Lip balm&cream") || category.contains("Skincare")
                    || category.contains("Body moisturiser") || category.contains("Eye cream")
                    || category.contains("Suncream") || category.contains("Hand cream&lotion")
                    || category.contains("Face moisturiser") || category.contains("Shower gel")) {
                ingredients.get(i).countSkinType(skinType);
                if (ifAcne) {
                    ingredients.get(i).countIfAcne();
                }
                if (ifDermatitis) {
                    ingredients.get(i).countIfDermatitis();
                }
            }

            if (ifAlcohols) {
                ingredients.get(i).countIfAlcohol();
            }

            if (ifSilicones) {
                ingredients.get(i).countIfSilicone();
            }
            if (ifBenzylDerivatives) {
                ingredients.get(i).countIfBenzylDerivative();
            }

            if (ifParabens) {
                ingredients.get(i).countIfParaben();
            }

            if (ifAnimalOrigin) {
                ingredients.get(i).countIfAnimalOrigin();
            }

            if (ifPopularAllergens) {
                ingredients.get(i).countIfPopularAllergen();
            }

            if (ifAllAllergens) {
                ingredients.get(i).countIfAllAllergen();
            }
            if (ifDangerousForPregnant) {
                ingredients.get(i).countIfDangerousForPregnant();
            }
            ifAllergyToIngredient = ingredients.get(i).checkIfUserAllergen(avoidAllergens);
            ifAllergy = ifAllergyToIngredient;
            ingredients.get(i).countIngredientAverage();
            String description = "";
            if (ifAllergyToIngredient) {
                description = new String(Character.toChars(0x1F61E)) + new String(Character.toChars(0x2757));
                ingredients.get(i).getIngredientInfo().add("You have allergy to this ingredient !");
            }
            else if (ingredients.get(i).getPoints() < 0.9) {
                description = new String(Character.toChars(0x1F620));
                numberOfBadIng++;
            } else if (ingredients.get(i).getPoints() > 1.1) {
                description = new String(Character.toChars(0x1F970));
                numberOfGoodIng++;
            } else {
                description = new String(Character.toChars(0x1F636));
                ingredients.get(i).getIngredientInfo().add("This ingredient doesn't have negative, nor positive influence on your body");
                numberOfNeutralIng++;
            }

            ingredientsNames.add(description+ " " + ingredients.get(i).getINCIname() );
            listIngredientDetails.put(ingredientsNames.get(i), ingredients.get(i).getIngredientInfo());

        }
        arrayIngredientsAdapter = new MyExpandableListAdapter(RecognizedIngredientsActivity.this, ingredientsNames, listIngredientDetails);
        productIngredients.setAdapter(arrayIngredientsAdapter);
        Double finalRate = 0.0;
        if(!ifAllergy) {
            finalRate = (double) 50 * ((numberOfGoodIng * 20) + numberOfNeutralIng) / ((numberOfGoodIng * 10) + (numberOfBadIng * 10) + numberOfNeutralIng);
        }
        productPoints.setText(finalRate.intValue() + "%");

    }


    public void getUserInformation() {
        hairType = ((String) ParseUser.getCurrentUser().get("hairType"));
        skinType = ((String) ParseUser.getCurrentUser().get("skinType"));

        ifAcne = (boolean) ParseUser.getCurrentUser().get("ifAcne");
        ifDermatitis = (boolean) ParseUser.getCurrentUser().get("ifDermatitis");
        ifDandruff = (boolean) ParseUser.getCurrentUser().get("ifDandruff");
        ifAlcohols = (boolean) ParseUser.getCurrentUser().get("ifAlcohols");
        ifSilicones = (boolean) ParseUser.getCurrentUser().get("ifSilicones");
        ifBenzylDerivatives = (boolean) ParseUser.getCurrentUser().get("ifBenzylDerivatives");
        ifParabens = (boolean) ParseUser.getCurrentUser().get("ifParabens");
        ifAnimalOrigin = (boolean) ParseUser.getCurrentUser().get("ifAnimalOrigin");
        ifPopularAllergens = (boolean) ParseUser.getCurrentUser().get("ifPopularAllergens");
        ifAllAllergens = (boolean) ParseUser.getCurrentUser().get("ifAllAllergens");
        ifDangerousForPregnant = (boolean) ParseUser.getCurrentUser().get("ifDangerousForPregnant");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("AvoidAllergen");
        query.selectKeys(Collections.singleton("AllergenName"));
        query.whereEqualTo("UserId",ParseUser.getCurrentUser().getObjectId());
        List<ParseObject> avoidAllergensObj = null;
        try {
            avoidAllergensObj = query.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert avoidAllergensObj != null;
        if(!avoidAllergensObj.isEmpty()){
            for(int i =0 ; i<avoidAllergensObj.size();i++){
                avoidAllergens.add(avoidAllergensObj.get(i).getString("AllergenName"));
            }
        }

    }
}
