package com.example.cosmeticspy;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;

public class AllergenPopupLayoutActivity extends AppCompatActivity {


    ListView listView;
    EditText editSearch;
    ArrayList<String> ingredients = new ArrayList<>();
    ArrayList<String> listForAdapter = new ArrayList<>();
    SearchAdapter arrayAdapterAll, arrayAdapterMy;
    Button searchButton;
    ArrayList<String> allergens_list  = new ArrayList<>();
    ArrayList<String> list_added  = new ArrayList<String>();
    ArrayList<String> list_deleted  = new ArrayList<String>();
    ParseQuery<ParseObject> ingredientsListquery;
    ParseQuery<ParseObject> avoidAllergenListquery;
    String mode;
    boolean searchFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allergen_popup_layout);

        listView = findViewById(R.id.list_view_with_checkbox);
        editSearch = findViewById(R.id.edit_search);

        ingredientsListquery = ParseQuery.getQuery("Ingredient");
        //ingredientsListquery.orderByAscending("INCIname");
        ingredientsListquery.selectKeys(Collections.singleton("INCIname"));
        ingredientsListquery.setLimit(30000);
        listView.setTextFilterEnabled(true);

        try {
            search_ingredients();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Bundle extras = getIntent().getExtras();
        mode = extras.getString("mode");



        if (mode.equals("edit")) {
            ParseUser user1 = ParseUser.getCurrentUser();
            String id = user1.getObjectId();
            avoidAllergenListquery = ParseQuery.getQuery("AvoidAllergen");
            avoidAllergenListquery.whereEqualTo("UserId", id);
            avoidAllergenListquery.findInBackground((currentAllergens, e) -> {
                if (e == null) {
                    for (int i = 0; i < currentAllergens.size(); i++) {
                        String allergen = currentAllergens.get(i).getString("AllergenName");
                        allergens_list.add(allergen);
                    }
                } else {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    public void search_ingredients() throws InterruptedException {
        long startTime = System.currentTimeMillis();
        final App application = (App) getApplicationContext();

        ArrayList<ParseObject> objectsX = application.getAllIngredientsAll();

        for (int i = 0; i < objectsX.size(); i++) {
            ingredients.add(objectsX.get(i).getString("INCIname"));
        }

        listForAdapter.addAll(ingredients);
        if(arrayAdapterAll != null){
            arrayAdapterAll.clear();
        }

        arrayAdapterAll = new SearchAdapter(this, 0, listForAdapter){
            @Override
            public View getView(int position, View convertView, ViewGroup parent)
            {
                View row;

                if (null == convertView) {
                    row = getLayoutInflater().inflate(android.R.layout.simple_list_item_1, null);
                } else {
                    row = convertView;
                }
                TextView tv = (TextView) row.findViewById(android.R.id.text1);
                tv.setText(Html.fromHtml((String) getItem(position)));
                return row;
            }
        };
        listView.setAdapter(arrayAdapterAll);

        searchFinished = true;

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String[] separated_term = s.toString().split(" ");
                StringBuilder term= new StringBuilder();
                for(int i=0; i<separated_term.length; i++){
                    if(!separated_term[i].equals("")){
                        term.append(separated_term[i]);
                        if(i<separated_term.length-1){
                            term.append(" ");
                        }
                    }
                }
                arrayAdapterAll.getFilter().filter(term.toString());
                arrayAdapterAll.notifyDataSetChanged();
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        listView.setOnItemClickListener((parent, view, position, id) -> {
            String ingredient_name = (String) listView.getItemAtPosition(position);
            showDialog(AllergenPopupLayoutActivity.this, ingredient_name, "What do you want to do?");
        });

    }

    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create(); //Read Update
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton( Dialog.BUTTON_POSITIVE, "add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (!allergens_list.contains(title)) {
                    allergens_list.add(title);
                    list_added.add(title);
                    list_deleted.remove(title);
                    final App application = (App) getApplicationContext();
                    application.setIfAllergyUpdate(true);
                }
            }
        });

        alertDialog.setButton( Dialog.BUTTON_NEUTRAL, "delete", new DialogInterface.OnClickListener()    {
            public void onClick(DialogInterface dialog, int which) {
                if (allergens_list.contains(title)) {
                    allergens_list.remove(title);
                    list_deleted.add(title);
                    list_added.remove(title);
                    final App application = (App) getApplicationContext();
                    application.setIfAllergyUpdate(true);
                }
            }

        });

        alertDialog.setCancelable(true);
        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                if (allergens_list.contains(title)) {
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.light_grey));
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }
                else {
                    alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.light_grey));
                    alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setEnabled(false);
                }
            }
        });
        alertDialog.show();
        Button deleteButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
    }

    public void my_allergens(View view) {
        for (String element: allergens_list) {
            System.out.println(element);
        }
        if (searchFinished) {
            arrayAdapterAll.clear();
            if (allergens_list != null){
                int i = 0;
                for (String element : allergens_list) {
                    arrayAdapterAll.insert(element, i);
                    i = i + 1;
                }
            }
            editSearch.setText("");
        }
        else {Toast.makeText(this, "Wait", Toast.LENGTH_SHORT).show();}
        editSearch.setText("");
    }

    public void all_allergens(View view) {
        if (searchFinished) {
            arrayAdapterAll.clear();
            if (ingredients != null) {
                int i = 0;
                for (String element : ingredients) {
                    arrayAdapterAll.insert(element, i);
                    i = i + 1;
                }
            }
            editSearch.setText("");
        }
        else {Toast.makeText(this, "Wait", Toast.LENGTH_SHORT).show();}
    }

    public void delete_all (View view) throws InterruptedException {
        final int[] conf = {0};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //dialog.dismiss();
                conf[0] = 1;
                if (searchFinished) {
                    list_deleted.addAll(allergens_list);
                    allergens_list.clear();
                    final App application = (App) getApplicationContext();
                    application.setIfAllergyUpdate(true);
                }
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                conf[0] = -1;
                //dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

        final Button positiveButton = alert.getButton(AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButtonLL.gravity = Gravity.CENTER;
        positiveButtonLL.weight = (float) 0.75;
        positiveButtonLL.leftMargin = 20;
        positiveButton.setLayoutParams(positiveButtonLL);

        final Button negativeButton = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams negativeButtonLL = (LinearLayout.LayoutParams) negativeButton.getLayoutParams();
        negativeButtonLL.gravity = Gravity.CENTER;
        negativeButtonLL.weight = (float) 0.75;
        negativeButtonLL.rightMargin = 20;
        negativeButton.setLayoutParams(negativeButtonLL);
    }

    public void ok(View view) {
        Intent retData=new Intent();
        retData.putExtras(retData.putExtra("list_of_allergens", allergens_list));
        retData.putExtras(retData.putExtra("list_added", list_added));
        retData.putExtras(retData.putExtra("list_deleted", list_deleted));
        retData.putExtras(retData.putExtra("result", "ok"));
        setResult(RESULT_OK, retData);
        finish();
    }

    public void cancel(View view) {
        Intent retData=new Intent();
        retData.putExtras(retData.putExtra("result", "cancel"));
        setResult(RESULT_CANCELED, retData);
        finish();
    }

}